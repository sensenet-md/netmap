package netmap.util;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tcb.netmap.util.PortUtil;

public class PortUtilTest {

	private List<ServerSocket> socketTrash;
	private Integer port1;
	private Integer port2;
	private Integer port3;

	@Before
	public void setUp() throws Exception {
		this.socketTrash = new ArrayList<>();
		this.port1 = 22134;
		this.port2 = port1 + 1;
		this.port3 = port2 + 1;
	}
	
	@After
	public void tearDown() throws Exception {
		for(ServerSocket s:socketTrash) closeSocket(s);
	}
	
	private void closeSocket(ServerSocket socket){
		try{socket.close();} catch(IOException e){};
	}
	
	private ServerSocket createSocket(Integer port) throws IOException {
		ServerSocket s = new ServerSocket(port);
		socketTrash.add(s);
		return s;
	}

	@Test
	public void testGetFreePort() {
		Integer p = PortUtil.getNextFreePort(port1, port2);
		assertEquals(port1,p);
		assertCreateSocket(p);
	}
		
	private void assertCreateSocket(Integer port) {
		try{
			createSocket(port);
		} catch(IOException e){
			fail("Port is not free");
		}
		
	}
	
	@Test
	public void testGetFreePortWhenFirstBlocked() throws IOException {
		Integer p = PortUtil.getNextFreePort(port1, port3+1);
		assertEquals(port1,p);
		
		assertCreateSocket(port1);
		assertCreateSocket(port2);
		
		p = PortUtil.getNextFreePort(port1, port3+1);
		
		assertEquals(port3,p);
		assertCreateSocket(p);
	}

}
