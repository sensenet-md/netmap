package netmap.structureViewer.pymol;

import static org.junit.Assert.*;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.limiter.TooManyItemsException;

import netmap.structureViewer.AbstractViewerTest;

@Ignore
public class PymolViewerTest extends AbstractViewerTest {
	private static final Path testSessionPath = Paths.get("structureViewer","test.pse");
	
	protected Path testTrajPath = Paths.get("structureViewer", "2ala.dcd");
	private static final String pymolCommand = "pymol2";
		
	private final String getResource(Path path){
		return getClass().getClassLoader().getResource(path.toString()).getPath();
	}
	
	@Override
	protected String getTestPdbPath() {
		return getResource(testPdbPath);
	}

	@Override
	protected String getTestTopPath() {
		return getResource(testTopPath);
	}

	@Override
	protected String getTestTrajPath() {
		return getResource(testTrajPath);
	}
	
	@Override
	protected StructureViewer getViewer() throws IOException {
		List<String> args = Arrays.asList(pymolCommand, "-c", "-K");
		return new PymolViewerFactory(args).createViewer();
	}
	
	// Does not shutdown properly. 
		// Can cause memory leak in root fs when logging.DEBUG is enabled in init_viewer.py
	@Ignore
	@Test
	public void testStartWithSession() throws IOException, TooManyItemsException {
		//String pymolCmd = "pymol";
		//StructureViewer viewer = new PymolViewerFactory(
		//		pymolCommand,100,100,Optional.of(testSession)).createViewer();
		//Residue r = new ResidueImpl(100, "", "", "", "A");
		//viewer.showResidues("test", Arrays.asList(r));
	}
	
	@Test(expected=IOException.class)
	public void testTranslateWhitespace()  throws IOException {
		String modelName = "a b c";
		
		viewer.loadModel(testPdb, "pdb", modelName);
		
	}
	
}
