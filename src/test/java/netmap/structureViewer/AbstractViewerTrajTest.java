package netmap.structureViewer;

import static org.junit.Assert.*;


import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollection;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.chimera.ChimeraViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.InitBlocker;
import com.tcb.netmap.util.limiter.TooManyItemsException;
import com.tcb.common.util.Combinatorics;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;


public abstract class AbstractViewerTrajTest {
	
	protected String alaPathBase = Paths.get("structureViewer", "2ala").toString();
	protected String ubqPathBase = Paths.get("structureViewer", "ubq").toString();
	
	protected Map<String,String> paths;
	private StructureViewer viewer;
	
	protected abstract FormatCollection getTopologyFormatCollection();
	protected abstract FormatCollection getTrajectoryFormatCollection();
		
	protected abstract StructureViewer createViewer() throws IOException;
	
	protected class TestCombination{
		public FileFormat topFormat;
		public FileFormat trajFormat;
		public String pathBase;

		public TestCombination(FileFormat topFormat, FileFormat trajFormat, String pathBase) {
			this.topFormat = topFormat;
			this.trajFormat = trajFormat;
			this.pathBase = pathBase;
		}
	};
	
	
	@Before
	public void setUp() throws IOException {
		this.viewer = createViewer();
		InitBlocker.waitMaxSeconds(viewer::isActive, 20);
	}
	
	@After
	public void tearDown() throws IOException {
		this.viewer.kill();
	}
	
	protected String getKey(FileFormat topFormat, FileFormat trajFormat){
		return topFormat.getStandardName() + "_" + trajFormat.getStandardName();
	}
	
	@Test
	public void testLoadTraj(){
		List<Triple<Exception,String,String>> errors = new ArrayList<>();
		for(TestCombination comb:getTestCombinations()){
			FileFormat topFormat = comb.topFormat;
			FileFormat trajFormat = comb.trajFormat;
			
			String topFormatName = topFormat.getStandardName();
			String topPath = getResource(comb.pathBase + "." + topFormatName);
			
			String trajFormatName = trajFormat.getStandardName();
			String trajPath = getResource(comb.pathBase + "." + trajFormatName);
			System.out.print(String.format("Testing: %s,%s ...", topFormatName, trajFormatName));
			try {
				viewer.loadModel(topPath, topFormatName, "test");
				viewer.loadTraj("test", trajPath, trajFormatName);
				viewer.showModel("test");
				System.out.println("OK");
			} catch(Exception e){
				Triple<Exception,String,String> err = new ImmutableTriple<>(
						e,
						topFormat.getStandardName(),
						trajFormat.getStandardName());
				errors.add(err);
				System.out.println("Error");
			} 
		}
			
		if(!errors.isEmpty()){
			fail("Reported errors");
			for(Triple<Exception,String,String> e:errors){
				String message = String.format("%s,%s,%s", e.getMiddle(), e.getRight(), e.getLeft());
				System.out.println(message);
			}
		}
	}
	
	protected String getResource(String path){
		URL resource = getClass().getClassLoader().getResource(path);
		if(resource==null) throw new IllegalArgumentException("Could not find resource: " + path);
		return resource.getPath();
	}
	

	protected List<TestCombination> getTestCombinations() {
		List<FileFormat> topFormats = getTopologyFormatCollection().getOptions();
		List<FileFormat> trajFormats = getTrajectoryFormatCollection().getOptions();
		List<Tuple<FileFormat,FileFormat>> combs = Combinatorics.getCartesianProduct(topFormats,trajFormats);
		List<TestCombination> results = new ArrayList<>();
		for(Tuple<FileFormat,FileFormat>c:combs){
			results.add(new TestCombination(c.one(),c.two(),alaPathBase));
		}
		return results;
	}
	
}
