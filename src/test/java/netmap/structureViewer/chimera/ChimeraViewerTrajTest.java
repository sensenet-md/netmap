package netmap.structureViewer.chimera;

import static org.junit.Assert.*;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollection;
import static com.tcb.netmap.fileFormat.TopologyFileFormat.*;
import static com.tcb.netmap.fileFormat.TrajectoryFileFormat.*;
import com.tcb.netmap.fileFormat.chimera.ChimeraTopologyFormatCollection;
import com.tcb.netmap.fileFormat.chimera.ChimeraTrajectoryFormatCollection;
import com.tcb.netmap.fileFormat.pymol.PymolTopologyFormatCollection;
import com.tcb.netmap.fileFormat.pymol.PymolTrajectoryFormatCollection;
import com.tcb.netmap.fileFormat.vmd.VmdTopologyFormatCollection;
import com.tcb.netmap.fileFormat.vmd.VmdTrajectoryFormatCollection;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.chimera.ChimeraViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.limiter.TooManyItemsException;
import com.tcb.common.util.Tuple;

import netmap.structureViewer.AbstractViewerTest;
import netmap.structureViewer.AbstractViewerTrajTest;

@Ignore
public class ChimeraViewerTrajTest extends AbstractViewerTrajTest {

	private static final String command = "chimera";

	@Override
	protected FormatCollection getTopologyFormatCollection() {
		return new ChimeraTopologyFormatCollection();
	}

	@Override
	protected FormatCollection getTrajectoryFormatCollection() {
		return new ChimeraTrajectoryFormatCollection();
	}

	@Override
	protected StructureViewer createViewer() throws IOException {
		List<String> args = Arrays.asList(command);
		return new ChimeraViewerFactory(args).createViewer();

	}
	
	@Override
	protected List<TestCombination> getTestCombinations() {
		List<TestCombination> c = new ArrayList<>();
		c.add(new TestCombination(PRMTOP, NC, alaPathBase));
		c.add(new TestCombination(PRMTOP, DCD, alaPathBase));
		c.add(new TestCombination(PSF, DCD, alaPathBase));
		return c;
	}
	

	


}
