package netmap.structureViewer.vmd;

import static org.junit.Assert.*;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.fileFormat.FormatCollection;
import com.tcb.netmap.fileFormat.vmd.VmdTopologyFormatCollection;
import com.tcb.netmap.fileFormat.vmd.VmdTrajectoryFormatCollection;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.chimera.ChimeraViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.limiter.TooManyItemsException;

import netmap.structureViewer.AbstractViewerTest;
import netmap.structureViewer.AbstractViewerTrajTest;

@Ignore
public class VMDViewerTrajTest extends AbstractViewerTrajTest {

	private static final String command = "vmd";

	@Override
	protected FormatCollection getTopologyFormatCollection() {
		return new VmdTopologyFormatCollection();
	}

	@Override
	protected FormatCollection getTrajectoryFormatCollection() {
		return new VmdTrajectoryFormatCollection();
	}

	@Override
	protected StructureViewer createViewer() throws IOException {
		List<String> args = Arrays.asList(command, "-dispdev", "text");
		return new VMDViewerFactory(args).createViewer();

	}
		
	

	


}
