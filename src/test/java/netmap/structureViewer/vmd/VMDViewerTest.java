package netmap.structureViewer.vmd;

import static org.junit.Assert.*;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.limiter.TooManyItemsException;

import netmap.structureViewer.AbstractViewerTest;

@Ignore
public class VMDViewerTest extends AbstractViewerTest {

	protected Path testTrajPath = Paths.get("structureViewer", "2ala.dcd");
	private static final String vmdCommand = "vmd";
		
	private final String getResource(Path path){
		return getClass().getClassLoader().getResource(path.toString()).getPath();
	}

	@Override
	protected String getTestPdbPath() {
		return getResource(testPdbPath);
	}

	@Override
	protected String getTestTopPath() {
		return getResource(testTopPath);
	}

	@Override
	protected String getTestTrajPath() {
		return getResource(testTrajPath);
	}

	@Override
	protected StructureViewer getViewer() throws IOException {
		return new VMDViewerFactory(Arrays.asList(vmdCommand,"-dispdev","text")).createViewer();
	}
		
	@Override
	public void testZoomResidue()  throws IOException {
		// Ignore
	}
	
	@Override
	public void testZoomInteraction()  throws IOException {
		// Ignore
	}

	


}
