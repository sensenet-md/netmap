package netmap.structureViewer.vmd;


import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.InitBlocker;


public class VMDViewerTestManual {

	/* This class is for manual testing only and should not run during building/auto testing.
	 */
	private StructureViewer viewer;
	private Path testPdb;
	private String modelName;
	private Path testTop;
	private Path testTraj;

	private static final Path testPdbPath = Paths.get("vmd","test.pdb");
	
	private static final Path testTopPath = Paths.get("vmd", "2ala.prmtop");
	private static final Path testTrajPath = Paths.get("vmd", "2ala.dcd");
	
	@Before
	public void setUp() throws Exception {
		this.testPdb = getResource(testPdbPath);
		this.viewer = new VMDViewerFactory(Arrays.asList("vmd")).createViewer();
		InitBlocker.waitMaxSeconds(viewer::isActive,20);
		this.modelName = "test";
		
		this.testTop = getResource(testTopPath);
		this.testTraj = getResource(testTrajPath);
	}
	
	private Path getResource(Path path) throws Exception {
		InputStream in = getClass().getClassLoader()
				.getResourceAsStream(path.toString());
		File tmp = File.createTempFile("test","tmp");
		tmp.deleteOnExit();
		Files.copy(in, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return tmp.toPath();
	}

	@After
	public void tearDown() throws Exception {
		viewer.kill();
	}
	
	//@Ignore
	@Test
	public void run() throws Exception {
						
		
		//viewer.loadTraj(modelName, testTraj.toString(), "dcd");
		Residue r1 = Residue.create(100, "", "", "", "A");
		Residue r2 = Residue.create(101, "", "", "", "A");
		
		Atom a1 = Atom.create("N", r1);
		Atom a2 = Atom.create("O", r2);
		
		Interaction in = Interaction.create(
				a1,a2,
				new ArrayList<>(),
			    Timeline.create(Arrays.asList(1)),
				"None");
		
		try{
			viewer.loadModel(testPdb.toString(), "pdb", modelName);
			viewer.showModel(modelName);
			//viewer.setFrame(modelName, 60);

			viewer.showResidues(modelName, Arrays.asList(r1));
			//viewer.showResidues(modelName, Arrays.asList(r1));
			//viewer.showResidues(modelName, Arrays.asList(r2));
			//viewer.zoom(modelName, r1);
			//viewer.showInteractions(modelName, Arrays.asList(in));
			//viewer.hideInteractions(modelName, Arrays.asList(in));
			
			//viewer.showInteraction(modelName, in);
			
			viewer.zoomResidues(modelName, Arrays.asList(r1));
			//viewer.hideResidues(modelName, Arrays.asList(r1));
			//viewer.hideResidues(modelName, Arrays.asList(r2));
			//viewer.zoomResidues(modelName, Arrays.asList(r1,r2));
		} catch(Exception e){
			e.printStackTrace();
		}
		
		while(viewer.isActive()){
		}
		
	}
	
}
