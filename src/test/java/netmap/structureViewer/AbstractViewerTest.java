package netmap.structureViewer;

import static org.junit.Assert.*;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.pymol.PymolViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDViewerFactory;
import com.tcb.netmap.util.InitBlocker;
import com.tcb.netmap.util.limiter.TooManyItemsException;



public abstract class AbstractViewerTest {

	protected StructureViewer viewer;
	protected  String testPdb;
	protected  String modelName;
	protected  String testSession;
	protected  String pymolCmd;
	protected  String testTop;
	protected  String testTraj;
	protected Residue r1;
	protected Residue r2;
	protected Atom a1;
	protected Atom a2;
	protected Interaction in;
	
	protected Path testPdbPath = Paths.get("structureViewer","test.pdb");
	
	protected Path testTopPath = Paths.get("structureViewer", "2ala.prmtop");
	
		
	protected abstract String getTestPdbPath();
	protected abstract String getTestTopPath();
	protected abstract String getTestTrajPath();
	protected abstract StructureViewer getViewer() throws IOException ;
	
	protected String getTestTopFormat(){
		return FilenameUtils.getExtension(getTestTopPath());
	}
	
	protected String getTestTrajFormat(){
		return FilenameUtils.getExtension(getTestTrajPath());
	}
	
	@Before
	public void setUp() throws Exception {
		this.testPdb = getTestPdbPath();
		this.testTop = getTestTopPath();
		this.testTraj = getTestTrajPath();
		this.viewer = getViewer();
		InitBlocker.waitMaxSeconds(viewer::isActive,20);
		this.modelName = "test";
		this.r1 = Residue.create(100, "", "", "", "");
		this.r2 = Residue.create(101, "", "", "", "");
		this.a1 = Atom.create("N", r1);
		this.a2 = Atom.create("O", r2);
		this.in = Interaction.create(
				a1,a2,
				new ArrayList<>(),
				Timeline.create(Arrays.asList(1)),
				"None");
	}
		
	@After
	public void tearDown() throws Exception {
		viewer.kill();
	}
	
	@Test
	public void testShowModel() throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
	}

	@Ignore("Not implemented")
	@Test
	public void testHideModel() throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
		viewer.hideModel(modelName);
	}

	@Test
	public void testLoadModel()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
	}

	@Test
	public void testDeleteModel()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.deleteModel(modelName);		
	}


	@Ignore("Not implemented")
	@Test
	public void testCenter()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.center(modelName);
	}


	@Test
	public void testZoomResidue()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
		
		viewer.zoomResidues(modelName, Arrays.asList(r1));
	}
	
	@Test
	public void testZoomInteraction()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
		viewer.zoomInteractions(modelName, Arrays.asList(in));
	}


	@Ignore("Not implemented")
	@Test
	public void testUndoZoom()  throws IOException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.zoomResidues(modelName, Arrays.asList(r1));
		viewer.undoZoom();
	}
	
	@Test
	public void testShowInteraction()  throws IOException, TooManyItemsException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);

		viewer.showInteractions(modelName, Arrays.asList(in));
	}
	
	@Test
	public void testHideInteraction()  throws IOException, TooManyItemsException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
		
		viewer.showModel(modelName);
		viewer.showInteractions(modelName, Arrays.asList(in));
		viewer.hideInteractions(modelName, Arrays.asList(in));
	}
	
	@Test
	public void testLoadTraj() throws IOException, TooManyItemsException {
		viewer.loadModel(testTop, getTestTopFormat(),modelName);
		viewer.loadTraj(modelName, testTraj, getTestTrajFormat());
		viewer.showModel(modelName);
		
		viewer.showResidues(modelName, Arrays.asList(r1));
	}


	@Test
	public void testSetFrame() throws IOException {
		viewer.loadModel(testTop, getTestTopFormat(),modelName);
		viewer.loadTraj(modelName, testTraj, getTestTrajFormat());
		viewer.showModel(modelName);
		
		viewer.setFrame(modelName, 10);
	}


	@Test
	public void testShowResidue()  throws IOException, TooManyItemsException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);
		viewer.showResidues(modelName, Arrays.asList(r1));
	}


	@Test
	public void testHideResidue()  throws IOException, TooManyItemsException {
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.showModel(modelName);

		viewer.showResidues(modelName, Arrays.asList(r1));
		viewer.hideResidues(modelName, Arrays.asList(r1));
	}
	
	@Test
	public void testGetModels()  throws IOException {
		List<String> refModels = Arrays.asList(
						"test",
						"test_2"
				);
		
		String modelName2 = modelName + "_2";
		viewer.loadModel(testPdb, "pdb",modelName);
		viewer.loadModel(testPdb, "pdb",modelName2);

		List<String> models = viewer.getModels();
		
		assertEquals(refModels, models);
	}
	
	
	
	

	

}
