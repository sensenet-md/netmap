package netmap.external.command;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.netmap.external.command.TclCommand;

public class TclCommandTest {

	private String fun;
	private List<String> args;
	private TclCommand cmd;

	@Before
	public void setUp() throws Exception {
		this.fun = "foo";
		this.args = Arrays.asList("\\bar");
		this.cmd = new TclCommand(fun,args);
	}

	@Test
	public void testTranslate() {
		assertEquals("foo \"\\\\bar\"",cmd.translate());
	}

}
