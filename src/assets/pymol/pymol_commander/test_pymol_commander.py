#!/usr/bin/env python3

from pymol_commander import PymolCommander
import pymol
from pymol import cmd
from pymol import stored
from time import sleep
pymol.finish_launching()



commander=PymolCommander()

#default hiding everything
#cmd.do("hide everything")


#commander.loadModel("test2.pdb","pdb")

#stickcolors=["gray","yellow","red","white","orange"]
#chaincolors=["green","yellow","red","white","orange"]

#commander.showModel("obj0",chaincolors,stickcolors)
#commander.zoom("obj0","not pol")
#sleep(1)
#commander.undoZoom()
#sleep(1)
#commander.hideModel("obj0")
#commander.showSticks("obj0","chain a","grey")
#commander.hideSticks("obj0","chain a")
#commander.showSpheres("obj0","chain a","grey")
#commander.hideSpheres("obj0","chain a")
#commander.center("chain a")
#commander.showConnection("obj0","F/ILE`861/CA","F/VAL`1080/CA","black")
#commander.hideConnection("obj0","F/ILE`861/CA","F/VAL`1080/CA")
#commander.deleteModel("obj0")
stored.commander = commander
