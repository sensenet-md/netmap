#!/usr/bin/env python3

from os import sep
from .interface import PymolCommander as Base
from pymol import cmd
from math import sqrt
import logging


listDelimiter = ' '
reservedChars = {listDelimiter}

logger = logging.getLogger('__main__')

def colorDistance(rgb1,rgb2):
    dr2,dg2,db2 = [pow(a-b,2) for (a,b) in zip(rgb1,rgb2)]
    rAverage = 0.5 * (rgb1[0] + rgb2[0])
    t1 = 2 * dr2
    t2 = 4 * dg2
    t3 = 3 * db2
    t4 = (1.0/256) * (rAverage * (dr2 - db2))
    dc = sqrt(t1 + t2 + t3 + t4)
    return dc

def getAllColors():
    colors = cmd.get_color_indices()
    colors = [(c[0],cmd.get_color_tuple(c[1])) for c in colors]
    return colors

colorDistances = {}

def memColorDistance(rgb1,rgb2):
    def getKey(rgb):
        return ','.join([str(x) for x in rgb])
    key1 = getKey(rgb1)
    key2 = getKey(rgb2)
    if key1 not in colorDistances:
        colorDistances[key1] = {}
    if key2 not in colorDistances[key1]:
        colorDistances[key1][key2] = colorDistance(rgb1,rgb2)
    return colorDistances[key1][key2]

def getClosestColor(rgbStr):
    rgb = [float(x) for x in rgbStr.split(';')]
    colors = cmd.get_color_indices()
    minDist = float('inf')
    minCol = None
    for c in colors:
        cRgb = [x*255 for x in cmd.get_color_tuple(c[1])]
        dist = memColorDistance(rgb,cRgb)
        if dist < minDist:
            minDist = dist
            minCol = c
    return minCol[0]

class OnceDict(object):
    def __init__(self):
        self.dic = {}

    def put(self, key, value):
        if key in self.dic: return
        self.dic[key] = value

    def pop(self, key, default):
        return self.dic.pop(key, default)


class ColorStorage(object):
    def __init__(self):
        self.colors = OnceDict()
        self.key = '(model,chain,resi,alt,name)'

    def save(self, model, selection):
        vars = {'colors':self.colors}
        cmd.iterate('({} & {})'.format(model, selection), 'colors.put({},color)'.format(self.key), space=vars)

    def pop(self, model, selection):
        vars = {'colors':self.colors}
        cmd.alter('({} & {})'.format(model,selection), 'color=colors.pop({},color)'.format(self.key), space=vars)
        cmd.recolor()

def getInteractionKey(model,atom1,atom2):
    atomId1 = cmd.id_atom(select(model,atom1))
    atomId2 = cmd.id_atom(select(model,atom2))
    return "{}_atomId{}-atomId{}".format(model,atomId1,atomId2)

def verifyNoReservedChars(string):
    if reservedChars.intersection(set(string)):
        raise ValueError("Model name may not contain whitespace")

def select(model,sel=''):
    result = 'model {}'.format(model)
    if sel:
        result += ' & {}'.format(sel)
    return result

class PymolCommander(Base):

    def __init__(self):
        self.zoomHistory=[]
        self.colorStorage = ColorStorage()
        self.interactions=set()
        self.maxInteractions = 10
        self.colorDistances = colorDistances

        #cmd.bg_color('white')
        cmd.set('depth_cue',0)


    def exit(self):
        cmd.quit()

    def getModels(self):
        objects = cmd.get_object_list('(all)')
        for o in objects: verifyNoReservedChars(o)
        return listDelimiter.join(objects)

    def showModel(self, model, chainColors, stickColors):
        cmd.hide('everything',select(model))
        cmd.show('cartoon', select(model))
        chains=[]
        for ch in cmd.get_chains(model):
            chains.append(ch)
        if len(chains) == 0: chains.append('') # Fix for old PyMol versions
        for i in range(0,len(chains)):
            colorIndex = min(i,len(chainColors)-1)
            self.setColor(model, "chain '{}'".format(chains[i]), chainColors[colorIndex])
            logger.debug(i)

        logger.debug(len(chains))
        logger.debug('Finished show model')
        self._showDefault(model)
        cmd.util.cnc(select(model))

    def setColor(self, model, sel, colorStr):
        #color = getClosestColor(colorStr)
        r,g,b = [hex(int(x))[2:].ljust(2,'0') for x in colorStr.split(';')]
        color = '0x{}{}{}'.format(r,g,b)
        cmd.color(color, select(model,sel))

    def resetColors(self, model):
        pass

    def _showDefault(self, model):
        cmd.show('spheres', select(model, 'inorganic'))
        cmd.show('sticks', select(model, 'organic'))

    def hideModel(self, model):
        cmd.hide('everything', select(model))

    def showSticks(self, model, sel, colorStr):
        self.colorStorage.save(model, sel)
        cmd.show('sticks', select(model,sel))
        self.setColor(model,'({} & {})'.format(select(model, sel), 'name C*'), colorStr)

    def hideSticks(self, model, sel):
        cmd.hide('sticks', select(model,sel))
        self.colorStorage.pop(model, sel)
        self._showDefault(model)

    def showSpheres(self, model, sel, color):
        cmd.show('spheres', select(model,sel))
        cmd.color(getClosestColor(color), select(model,sel))
        #cmd.do("util.cnc(\""+str(model)+" & " + str(sel)+"\")")

    
    def hideSpheres(self, model,sel):
        cmd.hide('spheres', select(model,sel))
        self._showDefault(model)

    def showConnection(self, model, atom1, atom2, colorStr):
        key = getInteractionKey(model,atom1,atom2)
        if(key in self.interactions): return
        cmd.distance(key, select(model,atom1), select(model,atom2))
        cmd.hide('label', key)
        self.interactions.add(key)
        self.colorConnection(model, atom1, atom2, colorStr)


    def hideConnection(self, model, atom1, atom2):
        key = getInteractionKey(model,atom1,atom2)
        if(key not in self.interactions): return
        cmd.delete(key)
        self.interactions.remove(key)

    def colorConnection(self, model, atom1, atom2, colorStr):
        key = getInteractionKey(model,atom1,atom2)
        if(key not in self.interactions): return
        color = getClosestColor(colorStr)
        cmd.color(color,key)


    def loadModel(self,path,form,model):
        verifyNoReservedChars(model)
        #if form == "pdb":
        if form:
            cmd.load(path, model, 0, form)
        else:
            cmd.load(path, model, 0)

    def loadTraj(self, model, path, form):
        if form:
            cmd.load_traj(path, object = model, plugin = form)
        else:
            cmd.load_traj(path, object = model)

    def setFrame(self, model, n):
        cmd.frame(int(n)+1)

    def deleteModel(self, model):
        cmd.delete(select(model))

    def center(self, model):
        cmd.center(select(model))

    def zoom(self, model, sel):
        cmd.zoom(select(model,sel), animate = 1, complete = 1, state = -1)
        self.zoomHistory.append(cmd.get_view())
        if len(self.zoomHistory) > 10:
            del self.zoomHistory[0]

    def undoZoom(self):
        try:
            cmd.set_view(self.zoomHistory[-1])
            del self.zoomHistory[-1]
        except:
            print("Number of undoZooms has been exhausted.")


