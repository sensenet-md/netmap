#!/usr/bin/env python2
import chimera
from interface import ChimeraCommander as Base
from chimera import runCommand as rc
from chimera.misc import getPseudoBondGroup
from chimera import openModels, numpyArrayFromAtoms, Molecule, selection, viewer, colorTable
from chimera import MaterialColor
import Midas
import Trajectory
from Movie.gui import MovieDialog
from collections import OrderedDict
from chimera.selection import OSLSelection, ItemizedSelection
from functools import partial
from math import log,sqrt
import Queue

import sys
import logging

logger = logging.getLogger('__main__')

queue = Queue.Queue()
models = OrderedDict()
pseudoBondCategory = 'tcb-connections'
listDelimiter = " "
trajModelFormatPrefix = "tcb-trajModel_"
colorDistances = {}

def getRGB(colorStr):
    return [float(x)/255.0 for x in colorStr.split(';')]

def singleton(coll):
    s = set(coll)
    if len(s) == 0: raise ValueError("No elements in singleton")
    if len(s) > 1: raise ValueError("More than one element in singleton: " + s)
    return list(s)[0]

def getModelId(model):
    if model not in models:
        for mol in openModels.list():
            if model == mol.name:
                models[model] = mol.id
                break
    if model not in models: raise ValueError("Could not find model: " + model)
    return models[model]

def getMol(model):
    return Midas.model(getModelId(model))

def selectModel(model):
    return '#{}'.format(getModelId(model))

def select(model, sel):
    result = []
    if '&' in sel: raise ValueError('Intersection not supported on selection')
    for s in sel.split('|'):
        result.append('{}{}'.format(selectModel(model), s))
    return '|'.join(result)

def getAtoms(model, sel):
    atoms = []
    for s in sel.split('|'):
        atoms.extend(OSLSelection(select(model, s)).atoms())
    return atoms

def getCarbonAtoms(model, sel):
    atoms = getAtoms(model,sel)
    atoms = [a for a in atoms if a.name.startswith('C')]
    return atoms

def getConnectionKey(model,a1,a2):
    return model + "_" + a1.oslIdent() + "-" + a2.oslIdent()

trajImportModules = {
    "Amber", "Gromacs",
    "Charmm", "Namdprmtopdcd",
    "Namdpsfdcd"
}

for m in trajImportModules:
    exec("import Trajectory.formats.{}".format(m))

trajTopFormats = {
    "prmtop",
    "tpr",
    "psf"
}

def getTrajImportModule(topForm, trajForm):
    if topForm in trajImportModules: return topForm
    if trajForm in trajImportModules: return trajForm
    if topForm == "prmtop" and trajForm == "nc": return "Amber"
    if topForm == "tpr" and trajForm in ["trr","xtc"]: return "Gromacs"
    if topForm == "prmtop" and trajForm == "dcd": return "Namdprmtopdcd"
    if topForm == "psf" and trajForm == "dcd": return "Namdpsfdcd"
    raise ValueError(
        "Could not load structure/trajectory combination.")


#rc('set showCofR')

class TopologyInput(object):
    def __init__(self,path,form):
        self.path = path
        self.form = form

class OnceDict(object):
    def __init__(self):
        self.dic = {}

    def put(self, key, value):
        if key in self.dic: return
        self.dic[key] = value

    def pop(self, key, default):
        return self.dic.pop(key, default)

class ColorStorage(object):
    def __init__(self):
        self.colors = OnceDict()

    def save(self, model, selection):
        atoms = getCarbonAtoms(model,selection)
        for a in atoms: self.colors.put(a.oslIdent(),a.color.rgba())


    def pop(self, model, selection):
        atoms = getCarbonAtoms(model,selection)
        for a in atoms:
            sel = ItemizedSelection(a)
            colorTup = self.colors.pop(a.oslIdent(),a.color.name())
            color = ','.join([str(x) for x in colorTup])
            Midas.color(color,sel)

class ChimeraCommander(Base):

    def __init__(self):
        self.executing = False
        self.colorStorage = ColorStorage()
        self.connections = {}
        self.topologyInputs = {}
        self.connectionGroup = getPseudoBondGroup(pseudoBondCategory)
        self.connectionGroup.lineWidth = 8
        self.connectionGroup.lineType = 2
        self.connectionDrawStyle = 0
        self.modelViewSize = {}
        chimera.triggers.addHandler("check for changes", self.runQueue, None)

    def runQueue(self, name, userData, data):
        if self.executing: return
        self.executing = True
        try:
            while True:
                f = queue.get(False)
                f()
        except Queue.Empty:
            return
        finally:
            self.executing = False

    def _waitForQueue(self):
        while (not queue.empty()) or self.executing:
            pass

    def _registerModel(self,model, mol):
        mol.name = model
        models[model] = mol.id
        self.modelViewSize[model] = viewer.viewSize

    def getModels(self):
        modelNames = [m.name for m in openModels.list()]
        return listDelimiter.join(modelNames)

    def _getChainIDs(self, model):
        mol = getMol(model)
        chainIds = list(set([a.residue.id.chainId for a in mol.atoms]))
        chainIds.sort()
        return chainIds

    def showModel(self, model, chainColors, stickColors):
        chainIds = self._getChainIDs(model)
        mol = getMol(model)
        chimera.colorByElement(mol,carbonNone = True)
        for i,c in enumerate(chainIds):
            colorStr=chainColors[min(i,len(chainColors)-1)]
            self.setColor(model, select(model,':*.{}'.format(c)), colorStr)

    def hideModel(self, model):
        Midas.undisplay(selectModel(model))
        Midas.unribbon(selectModel(model))

    def setColor(self, model, sel, colorStr):
        carbons = getCarbonAtoms(model,sel)
        self._colorAtoms(carbons,colorStr)

    def resetColors(self, model):
        pass

    def _colorAtoms(self, atoms, colorStr):
        rgb = getRGB(colorStr)
        color = ','.join([str(x) for x in rgb])
        Midas.color(color, ItemizedSelection(atoms))

    def showSticks(self, model, sel, colorStr):
        self.colorStorage.save(model,sel)
        Midas.display(select(model,sel))
        Midas.represent('stick', select(model,sel))
        carbons = getCarbonAtoms(model,sel)
        self._colorAtoms(carbons, colorStr)

    def hideSticks(self, model, sel): #no way to select by style.
        self.colorStorage.pop(model, sel)
        Midas.undisplay(select(model,sel))

    def showSpheres(self, model, sel, colorStr):
        #mol = Midas.model(int(model))
        #chimera.colorByElement(mol,carbonNone = True )
        Midas.represent('sphere', select(model,sel))
        self.setColor(model,sel,colorStr)

    def hideSpheres(self, model,sel):
        Midas.undisplay(select(model,sel))

    def showConnection(self, model, atom1, atom2, colorStr):
        a1 = singleton(OSLSelection(select(model,atom1)).atoms())
        a2 = singleton(OSLSelection(select(model,atom2)).atoms())

        connection = self.connectionGroup.newPseudoBond(a1,a2)
        r,g,b = getRGB(colorStr)

        connection.color = MaterialColor(r,g,b)
        connection.drawMode = self.connectionDrawStyle

        key = getConnectionKey(model,a1,a2)
        self.connections[key] = connection

    def hideConnection(self, model, atom1, atom2):
        a1 = singleton(OSLSelection(select(model,atom1)).atoms())
        a2 = singleton(OSLSelection(select(model,atom2)).atoms())
        key = getConnectionKey(model,a1,a2)
        if key not in self.connections: return

        connection = self.connections.pop(key)

        self.connectionGroup.deletePseudoBond(connection)

    def loadModel(self,path,form,model):
        if form in trajTopFormats:
            self._loadTrajModel(path,form,model)
            return
        mol = Midas.open(path, form)[0]
        self._registerModel(model,mol)

    def _loadTrajModel(self,path,form,model):
        self.topologyInputs[model] = TopologyInput(path,form)

    def loadTraj(self, model,path,form):
        def run():
            if model not in self.topologyInputs:
                raise ValueError("Could not find topology for: " + model)
            topInput = self.topologyInputs[model]
            trajImportModuleName = getTrajImportModule(topInput.form, form)
            inputs = [topInput.path,path]
            eval("Trajectory.formats.{}.loadEnsemble({}, 1, None, MovieDialog)"
                 .format(trajImportModuleName,inputs))

        queue.put(run)
        self._waitForQueue()
        mol = openModels.list()[-1]
        self._registerModel(model, mol)

    def deleteModel(self, model):
        def run():
            Midas.delete(selectModel(model))
            del models[model]
        queue.put(run)

    def center(self, model):
        Midas.center(selectModel(model))

    def zoom(self, model, sel):
        def spread(model, sel):
            def spread(coord):
                return abs(max(coord) - min(coord))
            atoms = getAtoms(model,sel)
            if len(atoms) == 0: return 1.0
            coords = [a.coord() for a in atoms]
            X = [c[0] for c in coords]
            Y = [c[1] for c in coords]
            Z = [c[2] for c in coords]
            spreads = [spread(X),spread(Y),spread(Z)]
            return max(spreads)

        def run(model, sel, n):
            rc('freeze')
            viewer.viewSize = self.modelViewSize[model]

            cameraCenter = viewer.camera.center

            rc('cofr {}'.format(select(model, sel)))
            cofr = chimera.openModels.cofr

            dx, dy, dz = [a - b for (a, b) in zip(cameraCenter, cofr)]

            frames = 50
            dist = sqrt(pow(dx, 2) + pow(dy, 2)) / frames

            rc('move {},{},{} {} {}; wait'.format(dx, dy, 0, dist, frames))

            curScale = float(viewer.scaleFactor)
            #self.targetScaleFactor = n
            fac = 1.00
            if n > curScale:
                fac += 0.05
            else:
                fac -= 0.05
            steps = int(log(float(n) / curScale, fac))

            if (steps == 0): return

            rc('scale {} {}; wait'.format(fac, steps))
            viewer.viewSize = self.modelViewSize[model]
            rc('wait')

        fac = 10
        if sel == 'all':
            sel = ':'
            fac = 1

        maxSpread = spread(model,sel)

        print "Spread: ", maxSpread

        fac = float(fac) / (maxSpread * 0.5)
        fac = min(10.,fac)
        fac = max(1.,fac)

        print "Factor: ", fac

        f = partial(run,model,sel,fac)

        queue.put(f)

        logger.debug('past')
        #return 0

    def setFrame(self, model, n):
        #Midas.move(0,0,0,n)
        rc('coordset #{} {}'.format(getModelId(model),n))

    def exit(self):
        rc('exit')