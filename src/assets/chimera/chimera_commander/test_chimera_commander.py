#!/usr/bin/env python2

#TODO Check if python3 can be used

from chimera_commander import ChimeraCommander
#import time
import unittest
commander=ChimeraCommander()

#default hiding everything
#cmd.do("hide everything")


commander.loadModel("test.pdb","pdb","test")

stickcolors=["gray","yellow","red","white","orange","hot pink"]
chaincolors=["green","yellow","red","white","orange"]

commander.showModel("test",chaincolors,stickcolors)                        #check
#commander.zoom("test",":13.")                                              #check, but not properly
#commander.undoZoom()                                                   #not implemented, Markus said not needed
#sleep(1)                                                               #doesnt work. model shows after script is finished. not earlier
#commander.hideModel("0")                                               #check
#commander.showSticks("0",":.a","grey")                                 #check
#commander.showSticks("test",":13.","grey")                                 #check
#commander.hideSticks("0",":.a")                                        #check
#commander.showSpheres("0",":.A","grey")                                #check
#commander.hideSpheres("0",":.A")                                       #check
#commander.center("0")                                                  #check
#commander.showConnection("0",":162.A@ca",":162.B@ca","black")          #color missing
#commander.hideConnection("0",":162.A@ca",":162.B@ca")                  #check
#commander.deleteModel("0")                                             #check
#comander.loadTraj("0",path)                                            #not working
commander.zoom("test",":10.")
