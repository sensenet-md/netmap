#!/bin/bash

set -eux
set -o pipefail

#Add command to run tests in chimera
chimera --script test_chimera_commander.py
