set scriptDir [file dirname [info script]]
set libDir [file join $scriptDir lib vmd-lib]

source [file join $libDir loadMolecules.tcl]
source [file join $libDir representations.tcl]
source [file join $libDir colors.tcl]
source [file join $libDir views.tcl]

variable models
variable selectedRepName
variable shownInteractionRepNames
variable colorDistances
variable customColorReps
variable defaultReps


proc _max {lst} {
    set a [lindex $lst 0]
    foreach i $lst {if {$i>$a} {set a $i}};return $a
}
proc _min {lst} {
    set a [lindex $lst 0]
    foreach i $lst {if {$i<$a} {set a $i}};return $a
}

proc _abs {x} {expr {abs($x)}}

proc _absLst {lst} {
    set result {}
    foreach e $lst {
	lappend result [_abs $e]
    }
    return $result
}

proc _getColors {} {
    set i 0
    foreach color [colorinfo colors] {
	lassign [colorinfo rgb $color] r g b
	set colors($i) [list $r $g $b]
	incr i
    }
    return [array get colors]
}

proc _colorDistance {rgb1 rgb2} {
    set d2 []
    for {set i 0} {$i<3} {incr i} {
	set dI [expr [lindex $rgb1 $i] - [lindex $rgb2 $i]]
	lappend d2 [expr pow($dI,2)]
    }
    lassign $d2 dr2 dg2 db2
    set rAverage [expr 0.5 * ([lindex $rgb1 0] + [lindex $rgb2 0])]
    set t1 [expr 2 * $dr2]
    set t2 [expr 4 * $dg2]
    set t3 [expr 3 * $db2]
    set t4 [expr (1.0/256) * ($rAverage * ($dr2 - $db2))]
    set dc [expr sqrt($t1 + $t2 + $t3 + $t4)]
    return $dc    
}

proc _memColorDistance {rgb1 rgb2} {
    global colorDistances
    set key1 [join $rgb1 ","]
    set key2 [join $rgb2 ","]
    set key [join [list $key1 $key2] "_"]
    if {[expr ![info exists colorDistances($key)]]} {
	set colorDistances($key) [_colorDistance $rgb1 $rgb2]
    }
    return $colorDistances($key)
}

proc _getClosestColor {rgbStr} {
    set rgb [split $rgbStr ";"]
    set colors [_getColors]
    set minCol ""
    set minDist [expr Inf]
    foreach {cIndex cRgbRaw} $colors {
	set cRgb []
	foreach x $cRgbRaw {
	    lappend cRgb [expr $x * 255]
	}
	set dist [_memColorDistance $rgb $cRgb]
	if {$dist < $minDist} {
	    set minDist $dist
	    set minCol $cIndex
	}
    }
    return [lindex [colorinfo colors] $minCol] 
}


proc _setBeta {model selection value} {
    set sel [atomselect [_getId $model] $selection]
    $sel set beta $value
}

proc _setOcc {model selection value} {
    set sel [atomselect [_getId $model] $selection]
    $sel set occupancy $value
}

proc _getInteractionKey {model selection1 selection2} {
    set sel1Key $selection1
    set sel2Key $selection2
    return [format "%s-%s-%s" $model $sel1Key $sel2Key]
}

proc getModels {} {
    set models [molinfo list]
    set modelNames []
    foreach molId $models {
	lappend modelNames [molinfo $molId get name]
    }
    return $modelNames
}

proc _getId {model} {
    global models
    if {[expr ![info exists models($model)]]} {
	set modelList [molinfo list]
	foreach molId $modelList {
	    set molName [molinfo $molId get name]
	    if [string equal $model $molName] {
		set models($model) $molId
		break
	    }
	}
   }
   return $models($model)
}

proc _getRepId {model repName} {
    set molId [_getId $model]
    set repId [mol repindex $molId $repName]
    return $repId
}

proc _getSelectedRep {model} {
    global selectedRepName
    if {[expr ![info exists selectedRepName($model)]]} {return -1}
    set index [_getRep $model $selectedRepName($model)]
    return $index
}

proc _getRep {model repName} {
    set index [mol repindex [_getId $model] $repName]
    return $index
}

proc _select {model sel} {
    set selection [uplevel 1 atomselect [_getId $model] $sel]
    return $selection
}

proc _contains {lst ele} {
    return [lsearch -exact $lst $ele]
}

proc loadModel {path form model} {
    global models
    global shownInteractionRepNames
    mol new $path type $form first 0 last 0 filebonds 1 autobonds 1 waitfor all
    set models($model) [molinfo top]
    mol rename top $model
    mol delrep 0 top
    _setBeta $model "all" 0
    _setOcc $model "all" 1
}

proc deleteModel {model} {
    mol delete [_getId $model]
}

proc loadTraj {model path form} {
    # Delete first frame in case there is already a model loaded
    animate delete beg 0 end 0 skip 0 [_getId $model]
    # molid must come last
    mol addfile $path type $form waitfor all molid [_getId $model]
}

proc showModel {model chainColors stickColors} {
    global defaultReps
    set chains [_getChains $model]
    set chainCount [llength $chains]
    set defaultProteinReps($model) []
    set defaultOtherReps($model) []
       
    for {set i 0} {$i < $chainCount} {incr i} {
	set chain [lindex $chains $i]
	set colorIndex [_min [list [expr $chainCount - 1] $i]]
	
	set selProtString [format "chain %s and protein and occupancy 1" $chain]
	set protColor [_getClosestColor [lindex $chainColors $colorIndex]]
	set protRep [addNewCartoonRep [_getId $model] $selProtString $protColor]
	lappend defaultReps($model) $protRep

	set selOtherString [format "chain %s and not (protein or solvent) and occupancy 1" $chain]
	set otherColor [_getClosestColor [lindex $stickColors $colorIndex]]
	set otherRep [addLicoriceRep [_getId $model] $selOtherString $otherColor]
	lappend defaultReps($model) $otherRep
    }
    _createSelectedRep $model
}

proc resetColors {model} {
    global customColorReps
    global defaultReps
    set molId [_getId $model]

    _setOcc $model "all" 1

    if {[expr ![info exists customColorReps($model)]]} {
	return
    }
    
    foreach repName $customColorReps($model) {
	set repId [_getRepId $model $repName]
	mol delrep $repId $molId
    }

    unset customColorReps($model)
}

proc setColor {model selection colorStr} {
    global customColorReps
    global defaultReps
    set molId [_getId $model]

    set color [_getClosestColor $colorStr]
    if {[expr ![info exists customColorReps($model)]]} {
	set customColorReps($model) []
    }
    set macroIndex [llength $customColorReps($model)]
    set macroName "colorSelection_$macroIndex"
    atomselect macro $macroName $selection
    set rep [addNewCartoonRep [_getId $model] $macroName $color]
    lappend customColorReps($model) $rep

    _setOcc $model $selection 0
}

proc _createSelectedRep {model} {
    global selectedRepName
    set selSelectedString "beta 1"
    set selectedRepName($model) \
        [addLicoriceRepAtomColor [_getId $model] $selSelectedString "Type"]
}

proc showSticks {model selection colorStr} {
    _setBeta $model $selection 1
    set repIndex [_getSelectedRep $model]
    set col [_getClosestColor $colorStr]
    if {$repIndex < 0} {_createSelectedRep $model}
    color Type C $col
}

proc hideSticks {model selection} {
    _setBeta $model $selection 0
}

proc showConnection {model selection1 selection2 colorStr} {
    global shownInteractionRepNames
    set key [_getInteractionKey $model $selection1 $selection2]
     if {[expr [info exists shownInteractionRepNames($key)]]} {
	return
    }
    set color [_getClosestColor $colorStr]
    set repName [addContactRep [_getId $model] [format "(%s) or (%s)" $selection1 $selection2] $color 0.2 100.0]
    set shownInteractionRepNames($key) $repName
}

proc hideConnection {model selection1 selection2} {
    global shownInteractionRepNames
    set key [_getInteractionKey $model $selection1 $selection2]
    if {[expr ![info exists shownInteractionRepNames($key)]]} {
	return
    }
    set repName $shownInteractionRepNames($key)
    set repIndex [_getRep $model $repName]
    if {$repIndex > -1} {
	mol delrep $repIndex [_getId $model]
	unset shownInteractionRepNames($key)
    }
}


proc _getChains {model} {
    set sel [_select $model "all"]
    puts $sel
    #set sel [atomselect [_getId $model] "all"]
    set chainAttrs [$sel get chain]
    set chains []
    foreach chain $chainAttrs {
	if {[_contains $chains $chain] < 0} { lappend chains $chain }
    }
    set chains [lsort $chains]
    return $chains
}

proc exit {} {
    quit
}

proc setFrame {model n} {
    animate goto $n
}

proc _zoomScaleMat {scaleF} {
    set m "{{$scaleF 0 0 0} {0 $scaleF 0 0} {0 0 $scaleF 0} {0 0 0 1}}"
    return $m
}

proc _zoomTransMat {} {
    set m "{{1 0 0 0} {0 1 0 0} {0 0 1 0} {0 0 0 1}}"
    return $m
}

proc _spreadLst {lst} {
    return [expr [_max $lst] - [_min $lst]]
}

proc _spread {model selection} {
    set sel [atomselect [_getId $model] $selection]
    set X [$sel get x]
    set Y [$sel get y]
    set Z [$sel get z]

    set spreadX [_spreadLst $X] 
    set spreadY [_spreadLst $Y] 
    set spreadZ [_spreadLst $Z]

    return [list $spreadX $spreadY $spreadZ]
}

proc _mult3DMatVec {mat vec} {
    set m {}
    for {set i 0} {$i<3} {incr i} {
	set row [lindex $mat $i]
	set result 0
	for {set j 0} {$j<3} {incr j} {
	    set result [expr $result + [lindex $row $j] * [lindex $vec $j]] 
	}
	lappend m $result		
    }
    return $m
}

proc _moveToVp {vpName} {
    # Small rotation seems to prevent domain error when moving
    rotate x by 0.01
    movetime_vp "here" $vpName 0.5
}

proc zoom {model selection} {
    set molId [_getId $model]
    set sel [atomselect $molId $selection]
    set spread [_spread $model $selection]
    set maxSpread [_max [_absLst $spread]]
    set maxSpread [_max [list $maxSpread 1.0]]
    set scaleFactor [expr 1 / $maxSpread]
    set scaleFactor [_min [list $scaleFactor 0.5]]
    set scaleFactor [_max [list $scaleFactor 0.02]]
    set selCenter [list [measure center $sel]]
    molinfo $molId set center "$selCenter"
    set vpName [next_vp_name]
    save_vp $vpName
    set vp [get_vp $molId]
    set vpNew [list \
		   [lindex $vp 0] \
		   [lindex $vp 1] \
		   [_zoomScaleMat $scaleFactor] \
		   [_zoomTransMat]]
    set frame [molinfo $molId get frame]
    set_vp $vpName $vpNew $frame
    _moveToVp $vpName
    remove_vp $vpName
}

