 # find out what color corresponds to which id:

proc initializeColorMap {} {
    global colormap
    if {[info exists colormap]} {return}
    array set colormap {}
    set i 0
    foreach color [colorinfo colors] {
	set colormap($color) $i
	incr i
    }
}

initializeColorMap

proc getColorID {name} {
    global colormap
    set id $colormap($name)
    if {$id eq ""} {
	puts "Color not found!"
    }
    return $id
}

proc toColorID {nameOrID} {
    if {[string is integer $nameOrID]} { return $nameOrID }
    return [getColorID $nameOrID]
}
