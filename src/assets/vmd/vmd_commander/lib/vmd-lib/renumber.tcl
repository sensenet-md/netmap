proc renum {mol offset} {
    
    set newNumbering {}
    set mySelection [atomselect $mol all]
    set oldNumbering [$mySelection get resid]
    foreach resid $oldNumbering {
	lappend newNumbering [expr {$resid + $offset}]
    }
    $mySelection set resid $newNumbering

}
