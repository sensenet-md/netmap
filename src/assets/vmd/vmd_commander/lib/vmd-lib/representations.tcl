set scriptDir [file dirname [info script]]
source [file join $scriptDir defaults.tcl]
source [file join $scriptDir colors.tcl]


proc addNewCartoonRep {molID selection color {size 0.3} } {
    global representation_resolution_default
    mol selection $selection
    mol color ColorID [toColorID $color]
    mol representation NewCartoon $size $representation_resolution_default 4.100000 0
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc addSurfaceRep {molID selection color} {
    mol selection $selection
    mol color ColorID [toColorID $color]
    mol representation MSMS 1.4 10
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc addLicoriceRep {molID selection color {size 0.3} } {
    global representation_resolution_default
    mol selection $selection
    mol color ColorID [toColorID $color]
    mol representation Licorice $size $representation_resolution_default $representation_resolution_default 
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc addVdWRep {molID selection color {size 1.0} } {
    global representation_resolution_default
    mol selection $selection
    mol color colorID [toColorID $color]
    mol representation VDW $size $representation_resolution_default 
    mol addrep $molID
    return [getMaxRepName $molID]
}



proc addLicoriceRepAtomColor {molID selection colorMethod {size 0.3}} {
    global representation_resolution_default
    mol selection $selection
    mol color $colorMethod
    mol representation Licorice $size $representation_resolution_default $representation_resolution_default 
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc addHbondRep {molID selection color {size 5.0} {distance 3.5} {angle 45}} {
    mol selection $selection
    mol color colorID [toColorID $color]
    mol representation HBonds $distance $angle $size
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc addContactRep {molID selection color {size 0.3} {distance 3.5}} {
    mol selection $selection
    mol color colorID [toColorID $color]
    mol representation DynamicBonds $distance $size
    mol addrep $molID
    return [getMaxRepName $molID]
}

proc labelAtomsByResidue {molID selection} {
    set i 0
    foreach a [[atomselect $molID $selection] list] {
	label add Atoms $molID/$a
	label textformat Atoms $i {%R%d}
	incr i
    }
}

proc getMolecules {} {
    set lastIndex [lindex [molinfo list] end]
    for {set i 0} {$i<=$lastIndex} {incr i} {
	dict set molecules $i [molinfo $i get filename]
    }
    return $molecules
}

proc getMaxRepID {molID} {
    return [expr {[molinfo $molID get numreps] - 1}] 
}

proc getMaxRepName {molID} {
    set maxRepID [getMaxRepID $molID]
    #dict set maxRep $maxRepID [mol repname $molID $maxRepID]
    return [mol repname $molID $maxRepID]
}

proc deleteRep {molID repName} {
    set repID [mol repindex $molID $repName]
    mol delrep $repID $molID
}
