proc loadPdb {pdbPath} {
    mol new $pdbPath first 0 last -1 filebonds 1 autobonds 1 waitfor all
    mol delrep 0 top
}

proc loadTraj {topolPath trajPath topType trajType first last step} {
	mol new $topolPath type $topType first 0 last -1 filebonds 1 autobonds 1 waitfor all
	mol addfile $trajPath type $trajType first $first last $last step $step filebonds 1 autobonds 1 waitfor all
	mol delrep 0 top
	}
