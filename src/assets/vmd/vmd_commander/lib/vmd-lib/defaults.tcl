
proc setDefault {var def} {
  global $var 
  if { ![info exists $var] } {
          set $var $def
    } 
}

setDefault material_default "EdgyShiny"
setDefault transparent_default "EdgyGlass"
setDefault representation_resolution_default 10.0

global material_default

mol default material $material_default
