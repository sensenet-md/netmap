proc render_tachyon_fast {outfile} {
    set cmd "/Applications/Debian/nodist/vmd/desktops/vmd-1.9.2/lib/tachyon_LINUXAMD64 -aasamples 12 -trans_max_surfaces 1 %s -format TARGA -o %s.tga"
    render Tachyon $outfile $cmd 
}

proc render_tachyon_pub {outfile} {
    set cmd "/Applications/Debian/nodist/vmd/desktops/vmd-1.9.2/lib/tachyon_LINUXAMD64 -res 4096 2160 -aasamples 24 -trans_max_surfaces 1 %s -format TARGA -o %s.tga"
    render Tachyon $outfile $cmd 
}

proc write_tachyon_scene {outfile} {
    render Tachyon $outfile ""
}
