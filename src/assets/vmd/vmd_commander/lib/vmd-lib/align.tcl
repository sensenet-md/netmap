proc alignToRef {thisMol refMol alignSelection} {
	
		set reference [atomselect $refMol $alignSelection frame 0]
		set compare [atomselect $thisMol $alignSelection]
                set allAtoms [atomselect $thisMol all]

                set num_steps [molinfo $thisMol get numframes]
                for {set frame 0} {$frame < $num_steps} {incr frame} {
                        $compare frame $frame
			$allAtoms frame $frame
                        set trans_mat [measure fit $compare $reference]
 			$allAtoms move $trans_mat
                }
	
}

proc alignToRefSel {thisMol refMol alignSelection refSelection} {
	
		set reference [atomselect $refMol $refSelection frame 0]
		set compare [atomselect $thisMol $alignSelection]
                set allAtoms [atomselect $thisMol all]

                set num_steps [molinfo $thisMol get numframes]
                for {set frame 0} {$frame < $num_steps} {incr frame} {
                        $compare frame $frame
			$allAtoms frame $frame
                        set trans_mat [measure fit $compare $reference]
 			$allAtoms move $trans_mat
                }
	
}


proc alignToThisFrameRefSel  {thisMol refMol alignSelection refSelection refFrame} {
	
		set reference [atomselect $refMol $refSelection frame $refFrame]
		set compare [atomselect $thisMol $alignSelection]
                set allAtoms [atomselect $thisMol all]

                set num_steps [molinfo $thisMol get numframes]
                for {set frame 0} {$frame < $num_steps} {incr frame} {
                        $compare frame $frame
			$allAtoms frame $frame
                        set trans_mat [measure fit $compare $reference]
 			$allAtoms move $trans_mat
                }
	
}


