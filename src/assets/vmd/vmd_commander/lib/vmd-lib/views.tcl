
proc get_vp {molid} {
  set M [list [molinfo $molid get center_matrix] [molinfo $molid get rotate_matrix] [molinfo $molid get scale_matrix] [molinfo $molid get global_matrix ] ]
  return $M
}

proc set_vp {viewname viewmatrix frame} {
  variable VCR::viewpoints
  set viewpoints($viewname,0) [lindex $viewmatrix 1]
  set viewpoints($viewname,1) [lindex $viewmatrix 0]
  set viewpoints($viewname,2) [lindex $viewmatrix 2]
  set viewpoints($viewname,3) [lindex $viewmatrix 3]
  set viewpoints($viewname,4) $frame
}

proc retrieve_vp {view_num} {
    VCR::retrieve_vp $view_num
}

proc move_vp {start end {morph_frames 50} args} {
    VCR::move_vp $start $end $morph_frames $args
}

proc save_vp {view_num} {
    VCR::save_vp $view_num
}

proc movetime_vp {start end time} {
    VCR::movetime_vp $start $end $time
}

proc list_vps {} {
    return [VCR::list_vps]
}

proc next_vp_name {} {
    return [expr [lindex [list_vps] end] + 1]
}

proc remove_vp {view_num} {
    VCR::remove_vp $view_num
}
