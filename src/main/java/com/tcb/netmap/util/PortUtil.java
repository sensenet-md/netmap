package com.tcb.netmap.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Random;

public class PortUtil {
	
	private static final Integer defaultLower = 49152;
	private static final Integer defaultHigher = 65535;
	
	public static Integer getNextFreePort(Integer lowerInclusive, Integer higherInclusive){
		for(int i=lowerInclusive;i<=higherInclusive;i++){
			ServerSocket s = null;
			try {
				s = new ServerSocket(i);
				s.close();
				return i;
			} catch (IOException e) {
				continue;
			} finally {
				if(s!=null)	try{s.close();} catch(IOException e){}	
			}
		}
		throw new RuntimeException("Could not find free port");
	}
	
	public static Integer getNextFreePort(){
		return getNextFreePort(defaultLower,defaultHigher);
	}
}
