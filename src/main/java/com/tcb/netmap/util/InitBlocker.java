package com.tcb.netmap.util;

import java.io.IOException;
import java.util.function.Supplier;

public class InitBlocker {
	public static void waitMaxMilliSeconds(Supplier<Boolean> untilTrue, int timeoutMillis){
			Integer millisPerStep = 10;
			int numSteps = (timeoutMillis / millisPerStep);
			if(numSteps < 1) 
				throw new IllegalArgumentException("Wait time must be larger than " + millisPerStep.toString());
			numSteps += 1;
			for(int i=0;i<numSteps;i++){
				if(untilTrue.get()) return;
				try{Thread.sleep(millisPerStep);} catch(InterruptedException e){e.printStackTrace();};
			}
			throw new RuntimeException("Initialization timeout");
	}
	
	public static void waitMaxSeconds(Supplier<Boolean> untilTrue, int timeoutSeconds){
		waitMaxMilliSeconds(untilTrue,timeoutSeconds*1000);
	}		
}
