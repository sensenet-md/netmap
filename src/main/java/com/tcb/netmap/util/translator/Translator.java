package com.tcb.netmap.util.translator;

public interface Translator {
	public String translate(String type);
}
