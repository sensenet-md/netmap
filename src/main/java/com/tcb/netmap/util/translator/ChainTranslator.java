package com.tcb.netmap.util.translator;

import java.util.List;

public class ChainTranslator implements Translator {

	private List<Translator> translators;

	public ChainTranslator(List<Translator> translators){
		this.translators = translators;
	}
	
	@Override
	public String translate(String s){
		for(Translator t:translators){
			s = t.translate(s);
		}
		return s;
	}
	
}
