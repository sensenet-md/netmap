package com.tcb.netmap.util.translator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

public class SingleTranslator implements Translator {
	private Set<String> in;
	private Function<String,String> out;

	public SingleTranslator(Collection<String> in, String out){
		this.in = new HashSet<>(in);
		this.out = (x) -> out;
	}
	
	public SingleTranslator(Collection<String> in, Function<String,String> out){
		this.in = new HashSet<>(in);
		this.out = out;
	}
	
	
	public String translate(String s){
		if(in.contains(s)) return out.apply(s);
		else return s;
	}
	
	public Boolean translatable(String s){
		return in.contains(s);
	}
}
