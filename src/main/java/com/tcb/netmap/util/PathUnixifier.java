package com.tcb.netmap.util;

import java.nio.file.Path;

import org.apache.commons.io.FilenameUtils;

public class PathUnixifier {
	public static String get(Path path){
		return FilenameUtils.separatorsToUnix(path.toString());
	}
}
