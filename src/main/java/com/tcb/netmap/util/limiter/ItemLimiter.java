package com.tcb.netmap.util.limiter;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


public class ItemLimiter<T> {
	private Integer max;
	private Set<T> shown;
	
	public ItemLimiter(Integer max){
		this.max = max;
		this.shown = ConcurrentHashMap.newKeySet();
	}
	
	public void add(Collection<T> items) throws TooManyItemsException {
		int endSize = shown.size() + items.size();
		if(endSize > max) throw new TooManyItemsException();
		items.forEach(i -> shown.add(i));
	}
	
	public void remove(Collection<T> items){
		items.forEach(i -> shown.remove(i));
	}
	
	public Boolean contains(T item){
		return shown.contains(item);
	}
	
	public void clear(){
		shown.clear();
	}
	
	public void setMax(Integer newMax){
		max = newMax;
	}
	
	public Integer getMax(){
		return max;
	}
	
	
}
