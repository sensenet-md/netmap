package com.tcb.netmap.external.answer;

import java.io.IOException;

public interface AnswerProtocol {
	public String parseAnswer(String message) throws IOException;

}
