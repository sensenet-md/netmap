package com.tcb.netmap.external.answer;

public class DefaultAnswerParser implements AnswerParser {

	private static final String listDelimiter = " ";
	
	@Override
	public String[] split(String answer) {
		return answer.split(listDelimiter);
	}

}
