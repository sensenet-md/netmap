package com.tcb.netmap.external.answer;

public interface AnswerParser {
	public String[] split(String answer);
}
