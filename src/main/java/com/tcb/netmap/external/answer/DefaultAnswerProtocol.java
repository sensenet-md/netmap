package com.tcb.netmap.external.answer;

import java.io.IOException;

public class DefaultAnswerProtocol implements AnswerProtocol {

	private static final String malformedAnswerResponse = "Malformed answer from application";
	
	@Override
	public String parseAnswer(String message) throws IOException {
		if(message==null || message.isEmpty()) {
			throw new IOException(malformedAnswerResponse);
		}
		
		String content = message.substring(1, message.length());
		if(message.startsWith("0")){
			return content;
		}
		else if(message.startsWith("1")){
			throw new IOException("Application signaled error: " + content);
		} else {
			throw new IOException(malformedAnswerResponse);
		}
	}

}
