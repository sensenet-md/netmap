package com.tcb.netmap.external;

import java.io.IOException;

public interface ExternalApplicationStarter {
	public ExternalApplication start() throws IOException;
}
