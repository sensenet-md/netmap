package com.tcb.netmap.external.command;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TclList {
	
	public static String translate(List<String> args){
		return "[list " + String.join(" ", quote(args)) + "]";
	}
	
	private static List<String> quote(List<String> args){
		return args.stream()
				.map(s -> "\"" + s + "\"")
				.collect(Collectors.toList());
	}
	
	public static String translate(String... args){
		return translate(Arrays.asList(args));
	}
	
}
