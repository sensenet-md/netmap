package com.tcb.netmap.external.command;

import java.io.IOException;
import java.util.List;

public interface Commander {
	public String sendAndReceive(String cmd, String... args) throws IOException ;
	public void send(String cmd, String... args) throws IOException ;
	public String stringify(List<String> lst);
}
