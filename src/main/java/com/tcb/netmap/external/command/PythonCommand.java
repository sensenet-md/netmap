package com.tcb.netmap.external.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PythonCommand implements Command {

	private String function;
	private List<String> arguments;
	
	public PythonCommand(String function, List<String> arguments){
		this.function = function;
		this.arguments = arguments;
	}
	
	public PythonCommand(String function, String... arguments){
		this(function, Arrays.asList(arguments));
	}
		
	public PythonCommand(String function){
		this(function, new ArrayList<>());
	}
	
	@Override
	public String translate() {
		List<String> quotedArgs = arguments.stream()
				.map(a -> quoteArg(a))
				.map(a -> convertBackslashes(a))
				.collect(Collectors.toList());
		return function + "(" + String.join(",", quotedArgs) + ")";
	}
	
	private String quoteArg(String arg){
		if(arg.startsWith("[")) return arg;
		else return "\"" + arg + "\"";
	}
	
	private String convertBackslashes(String arg){
		return arg.replaceAll("\\\\(?!\\\\)", "\\\\\\\\");
	}

}
