package com.tcb.netmap.external.command;

public interface Command {
	public String translate();
}
