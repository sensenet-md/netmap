package com.tcb.netmap.external.command;

import java.io.IOException;
import java.util.List;

import com.tcb.netmap.external.ExternalApplication;

public abstract class AbstractCommander implements Commander {
	protected abstract Command getCommand(String cmd, String... args);
		
	protected ExternalApplication app;
	
	private static final int timeout = 0;

	public AbstractCommander(ExternalApplication app){
		this.app = app;
	}
		
	@Override
	public String sendAndReceive(String cmd, String... args) throws IOException {
		Command command = getCommand(cmd,args);
		String answer = app.sendAndReceive(command.translate(), timeout);
		return answer;
	}
	
	@Override
	public void send(String cmd, String... args) throws IOException {
		Command command = getCommand(cmd,args);
		app.send(command.translate());
	}	
}
