package com.tcb.netmap.external;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import com.tcb.netmap.external.answer.AnswerProtocol;

public class ExternalApplicationImpl extends Thread implements ExternalApplication  {
	private volatile BufferedReader reader;
	private volatile BufferedWriter writer;
	private volatile Socket socket;
	private volatile Boolean started;
	private volatile Boolean stopped;
	
	private ServerSocket socketServer;
	private Process process;
	private Integer port;
	private AnswerProtocol answerProtocol;
	private StreamEater stdOutEater;
	private StreamEater stdErrEater;
	private String[] commands;

	private static final int bufSize = 64000;
	
	public ExternalApplicationImpl (
			Integer port,
			String[] commands,
			AnswerProtocol answerProtocol) throws IOException {
		this.commands = commands;
		this.started = false;
		this.stopped = false;
		this.port = port;
		this.answerProtocol = answerProtocol;
	}
	
	@Override
	public synchronized void start(){
		startSocket();
	}
	
	private void startSocket() {
		try{
			System.out.println("Starting external application: " + Arrays.toString(commands));
			this.socketServer = new ServerSocket(port, 0, InetAddress.getByName(null));
			socketServer.setReceiveBufferSize(bufSize);
		} catch(IOException e){
			kill();
			throw new RuntimeException("Error starting socket server: " + e.getMessage());
		}
		
		try{
			this.process = Runtime.getRuntime().exec(commands); // Start process with socket server
	    	this.stdOutEater = new StreamEater(process.getInputStream());
			stdOutEater.start();
			this.stdErrEater = new StreamEater(process.getErrorStream());
			stdErrEater.start();
		} catch(IOException e){
			kill();
			throw new RuntimeException("Error starting external application: " + e.getMessage());
		}
		
		try{
			this.socket = socketServer.accept();
			this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			this.started = true;
		} catch(IOException e) {
			kill();
			System.out.println("Error connecting socket with external application");
		}
	}

	@Override
	public synchronized void send(String message) throws IOException {
		checkStarted();
		try{
			while(!message.isEmpty()){
				int end = Math.min(bufSize,message.length());
				String block = message.substring(0, end);
				writer.write(block);
				writer.flush();	
				message = message.substring(end, message.length());
			} 
			writer.write('\n');
			writer.flush();
		} catch(IOException e){
			stopConnection();
			throw(e);
		}
	}
	
	private void checkStarted() throws IOException {
		if(started==false) throw new IOException("Socket not connected");
	}
			
	public synchronized String sendAndReceive(String message, int timeout) throws IOException {
		send(message);
		String answer = receiveMessage(timeout);
		answer = answerProtocol.parseAnswer(answer);
		return answer;
	}
	
	public String sendAndReceive(String message) throws IOException {
		return sendAndReceive(message,0);
	}
	
	private String receiveMessage(int timeout) throws IOException {
		try{
			socket.setSoTimeout(timeout);
			String line = null;
			while(line==null){
				line = reader.readLine();
			}
			return line;
		} catch(IOException e){
			stopConnection();
			throw(e);
		}	
	}
		
	public void stopConnection() {
		stopped = true;
		try{socketServer.close();} catch(Exception e){};
		try{stdOutEater.cancel();} catch(Exception e){};
		try{stdErrEater.cancel();} catch(Exception e){};
		try{socket.close();} catch(Exception e){};
	}
	
	@Override
	public void kill() {
		stopConnection();
		if(process!=null) process.destroyForcibly();
	}
	
	@Override
	public void finalize() {
		kill();
	}
		
	public Integer getPort(){
		return port;
	}

	@Override
	public Boolean isActive() {
		return started.equals(true) && socket.isConnected() && !socket.isClosed() && !stopped;
	}

	class StreamEater extends Thread {
		private InputStream is;
		private Boolean cancelled;

		public StreamEater(InputStream is){
			this.is = is;
			this.cancelled = false;
		}
		
		@Override
		public void run()
	    {
	        try
	        {
	            InputStreamReader isr = new InputStreamReader(is);
	            BufferedReader br = new BufferedReader(isr);
	            String line = br.readLine();
	            while (line != null) {
	            	System.out.println(line);
	            	line = br.readLine();
	            	if(cancelled) return;
	            }
	            	            	
	        } catch (IOException e) {
	                //e.printStackTrace();  
	       }
	    }
		
		public void cancel(){
			cancelled = true;
		}
		
	}

	
	
	
}
