package com.tcb.netmap.external;

import java.io.IOException;

public interface ExternalApplication {
	public String sendAndReceive(String message, int timeout) throws IOException;
	public String sendAndReceive(String message) throws IOException;
	public void send(String message) throws IOException;
	public void stopConnection();
	public void kill();
	public Integer getPort();
	public Boolean isActive();
	public void start();
}