package com.tcb.netmap.structureViewer;

import java.io.IOException;



public interface ViewerFactory {
	public StructureViewer createViewer() throws IOException;
}
