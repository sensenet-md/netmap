package com.tcb.netmap.structureViewer;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationImpl;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerParser;
import com.tcb.netmap.external.command.Command;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.external.command.PythonList;
import com.tcb.netmap.external.command.TclCommand;
import com.tcb.netmap.external.command.TclList;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.ViewerColor;
import com.tcb.netmap.structureViewer.ViewerColorSerializer;
import com.tcb.netmap.util.PortUtil;
import com.tcb.netmap.util.limiter.ItemLimiter;
import com.tcb.netmap.util.limiter.TooManyItemsException;
import com.tcb.netmap.util.translator.Translator;


public class StructureViewerImpl implements StructureViewer {
	
	private ExternalApplication app;
	private ItemLimiter<Residue> shownResiduesLimiter;
	private ItemLimiter<Interaction> shownInteractionsLimiter;
	private Commander commander;
	
	private Selecter selecter;
	private ViewerColorSerializer colorSerializer;
	private Translator typeTranslator;
	private AnswerParser answerParser;
		
	public StructureViewerImpl(
			ExternalApplication app,
			Selecter selecter,
			Commander commander,
			ViewerColorSerializer colorSerializer,
			Translator typeTranslator,
			AnswerParser answerParser,
			Integer maxShownResidues,
			Integer maxShownInteractions
			) {
		this.app = app;
		this.selecter = selecter;
		this.colorSerializer = colorSerializer;
		this.typeTranslator = typeTranslator;
		this.commander = commander;
		this.answerParser = answerParser;
		this.shownResiduesLimiter = new ItemLimiter<>(maxShownResidues);
		this.shownInteractionsLimiter = new ItemLimiter<>(maxShownInteractions);
	}
	
	@Override
	public void stopConnection() {
		try { commander.send("exit"); } catch(Exception e) {};
		app.stopConnection();		
	}
	
	@Override
	public void kill() {
		stopConnection();
		app.kill();		
	}
		
	@Override
	public List<String> getModels() throws IOException {
		String answer = commander.sendAndReceive("getModels");
		String[] models = answerParser.split(answer);
		return Arrays.asList(models);
	}

	@Override
	public Boolean isActive() {
		return app.isActive();
	}
	
	@Override
	public void showModel(String model, Collection<Color> chainColors) throws IOException {
		String colors = commander.stringify(
				chainColors.stream()
				.map(c -> colorSerializer.getColorString(c))
				.collect(Collectors.toList()));
		commander.sendAndReceive("showModel", model, colors, colors);		
	}
	
	@Override
	public void showModel(String model) throws IOException{
		showModel(model, this.getDefaultChainColors());		
	}

	@Override
	public void hideModel(String model) throws IOException {
		commander.sendAndReceive("hideModel", model);		
	}

	@Override
	public void loadModel(String path, String format, String model) throws IOException {
		format = typeTranslator.translate(format);
		commander.sendAndReceive("loadModel", path, format, model);	
	}

	@Override
	public void loadTraj(String model, String path, String format) throws IOException {
		format = typeTranslator.translate(format);
		commander.sendAndReceive("loadTraj", model, path, format);
	}

	@Override
	public void deleteModel(String model)  throws IOException {
		commander.sendAndReceive("deleteModel", model);		
	}

	@Override
	public void center(String model)  throws IOException {
		commander.sendAndReceive("center", model);		
	}

	@Override
	public void zoomModel(String model) throws IOException {
		commander.sendAndReceive("zoom", model, selecter.selectAll());
	}
	
	@Override
	public void zoomResidues(String model, Collection<Residue> residues)  throws IOException {
		if(residues.isEmpty()) return;
		String sel = selecter.selectResidues(residues);
		commander.sendAndReceive("zoom", model, sel);		
	}
	

	@Override
	public void zoomInteractions(String model, Collection<Interaction> interactions)  throws IOException {
		if(interactions.isEmpty()) return;
		String sel = selecter.selectInteractions(interactions);
		commander.sendAndReceive("zoom", model, sel);			
	}

	@Override
	public void undoZoom()  throws IOException {
		commander.sendAndReceive("undoZoom");		
	}

	@Override
	public void setFrame(String model, Integer n)  throws IOException {
		commander.sendAndReceive("setFrame", model, n.toString());		
	}

	@Override
	public void showResidues(String model, Collection<Residue> residues, Color color)  throws TooManyItemsException, IOException {
		if(residues.isEmpty()) return;
		shownResiduesLimiter.add(residues);
		String sel = selecter.selectResidues(residues);
		String colorStr = colorSerializer.getColorString(color);
		commander.sendAndReceive("showSticks", model, sel, colorStr);
	}
	

	@Override
	public void showResidues(String model, Collection<Residue> residues) throws TooManyItemsException, IOException {
		showResidues(model,residues,this.getDefaultSelectedColor());
	}
	
	@Override
	public void hideResidues(String model, Collection<Residue> residues)  throws IOException {
		residues = residues.stream()
				.filter(r -> shownResiduesLimiter.contains(r))
				.collect(Collectors.toList());
		if(residues.isEmpty()) return;
		String sel = selecter.selectResidues(residues);
		commander.sendAndReceive("hideSticks", model, sel);
		shownResiduesLimiter.remove(residues);
	}

	@Override
	public void showInteractions(String model, Collection<Interaction> interactions, Color color)  throws TooManyItemsException, IOException {
		String colorStr = colorSerializer.getColorString(color);
		interactions = interactions.stream()
				.filter(i -> !shownInteractionsLimiter.contains(i))
				.collect(Collectors.toList());
		shownInteractionsLimiter.add(interactions);
		for(Interaction interaction:interactions){
			String sourceSel = selecter.selectAtoms(Arrays.asList(interaction.getSourceAtom()));
			String targetSel = selecter.selectAtoms(Arrays.asList(interaction.getTargetAtom()));
			commander.sendAndReceive("showConnection",model,sourceSel,targetSel,colorStr);
		}
	}
	
	@Override
	public void showInteractions(String model, Collection<Interaction> interactions)
			throws TooManyItemsException, IOException {
		showInteractions(model,interactions,this.getDefaultInteractionColor());		
	}

	@Override
	public void hideInteractions(String model, Collection<Interaction> interactions)  throws IOException {
		interactions = interactions.stream()
				.filter(i -> shownInteractionsLimiter.contains(i))
				.collect(Collectors.toList());
		for(Interaction interaction:interactions){
			String sourceSel = selecter.selectAtoms(Arrays.asList(interaction.getSourceAtom()));
			String targetSel = selecter.selectAtoms(Arrays.asList(interaction.getTargetAtom()));
			commander.sendAndReceive("hideConnection",model,sourceSel,targetSel);
			shownInteractionsLimiter.remove(Arrays.asList(interaction));
		}
	}
	

	@Override
	public void colorResidues(String model, Collection<Residue> residues, Color color)
			throws IOException {
		if(residues.isEmpty()) return;
		String sel = selecter.selectResidues(residues);
		String colorStr = colorSerializer.getColorString(color);
		commander.sendAndReceive("setColor", model, sel, colorStr);
	}
	
	@Override
	public void resetColors(String model) throws IOException {
		commander.sendAndReceive("resetColors", model);		
	}
	
	@Override
	public void setMaxShownResidues(Integer max) {
		shownResiduesLimiter.setMax(max);		
	}

	@Override
	public void setMaxShownInteractions(Integer max) {
		shownInteractionsLimiter.setMax(max);		
	}

	@Override
	public Integer getMaxShownResidues(Integer max) {
		return shownResiduesLimiter.getMax();
	}

	@Override
	public Integer getMaxShownInteractions(Integer max) {
		return shownInteractionsLimiter.getMax();
	}




	



	

}
