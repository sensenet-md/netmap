package com.tcb.netmap.structureViewer;

import java.awt.Color;

public class DefaultColorSerializer implements ViewerColorSerializer {

	@Override
	public String getColorString(Color color) {
		return String.format("%d;%d;%d",color.getRed(),color.getGreen(),color.getBlue());
	}
	
}
