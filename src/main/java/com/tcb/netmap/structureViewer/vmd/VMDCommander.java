package com.tcb.netmap.structureViewer.vmd;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.command.AbstractCommander;
import com.tcb.netmap.external.command.Command;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.external.command.TclCommand;
import com.tcb.netmap.external.command.TclList;

public class VMDCommander extends AbstractCommander {

	public VMDCommander(ExternalApplication app){
		super(app);
	}
	
	@Override
	protected Command getCommand(String cmd, String... args){
		return new TclCommand(cmd,args);
	}

	@Override
	public String stringify(List<String> lst) {
		return TclList.translate(lst);
	}
	
}
