package com.tcb.netmap.structureViewer.vmd;

import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.util.translator.ChainTranslator;
import com.tcb.netmap.util.translator.SingleTranslator;
import com.tcb.netmap.util.translator.Translator;

public class VMDFileTypeTranslator extends ChainTranslator {

	public VMDFileTypeTranslator() {
		super(getTranslators());
	}

	private static List<Translator> getTranslators() {
		return Arrays.asList(
				new SingleTranslator(Arrays.asList("prmtop"),"parm7"),
				new SingleTranslator(Arrays.asList("nc"), "netcdf")
		);
	}
	
	
}
