package com.tcb.netmap.structureViewer.vmd;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerParser;
import com.tcb.netmap.external.answer.DefaultAnswerParser;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.structureViewer.Selecter;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.StructureViewerImpl;
import com.tcb.netmap.structureViewer.ViewerColorSerializer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.util.translator.Translator;

public class VMDViewerFactory implements ViewerFactory {

	private Integer maxShownResidues;
	private Integer maxShownInteractions;
	private List<String> args;
	private Optional<Path> sessionPath;

	public VMDViewerFactory(
			List<String> args,
			Integer maxShownResidues,
			Integer maxShownInteractions,
			Optional<Path> sessionPath){
		this.args = args;
		this.sessionPath = sessionPath;
		this.maxShownResidues = maxShownResidues;
		this.maxShownInteractions = maxShownInteractions;
	}
	
	public VMDViewerFactory(List<String> args){
		this(args,Integer.MAX_VALUE,Integer.MAX_VALUE,Optional.empty());
	}
	
	@Override
	public StructureViewer createViewer() throws IOException {
		ExternalApplication app = startApp();
		Selecter selecter = new VMDSelecter();
		Commander commander = new VMDCommander(app);
		ViewerColorSerializer colorSerializer = new VMDColorSerializer();
		Translator fileTypeTranslator = new VMDFileTypeTranslator();
		AnswerParser answerParser = new DefaultAnswerParser();
		return new StructureViewerImpl(
				app, selecter,
				commander, colorSerializer,
				fileTypeTranslator, answerParser,
				maxShownResidues, maxShownInteractions);
	}
	
	private ExternalApplication startApp() throws IOException {
		ExternalApplicationStarter starter = new VMDStarter(args,sessionPath);
		ExternalApplication app = starter.start();
		return app;
	}
}
