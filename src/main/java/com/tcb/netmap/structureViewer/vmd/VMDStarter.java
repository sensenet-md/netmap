package com.tcb.netmap.structureViewer.vmd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationImpl;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerProtocol;
import com.tcb.netmap.external.answer.DefaultAnswerProtocol;
import com.tcb.netmap.util.PathUnixifier;
import com.tcb.netmap.util.PortUtil;

public class VMDStarter implements ExternalApplicationStarter {
	
	private static final String initScriptName = "init_viewer.tcl";
	private static final String libName = "vmd.zip";
	private static final String resourcePath = "vmd";
	private static final String assetsPath = "assets";
	private static final String tempPrefix = "vmd";

	private List<String> baseArgs;
	private Optional<Path> sessionFile;
	
	public VMDStarter(List<String> args, Optional<Path> sessionFile){
		this.baseArgs = args;
		this.sessionFile = sessionFile;
	}
	
	@Override
	public ExternalApplication start() throws IOException {
		Path tempDir = new TempUtil(tempPrefix).createTempDir();
		Path initScript = Paths.get(tempDir.toString(), initScriptName);
		Path libZip = Paths.get(tempDir.toString(), libName);
		ResourceExtractor extractor = new ResourceExtractor(tempPrefix);
		
		extractor.extract(
				Paths.get(resourcePath, initScriptName),
				initScript);
		extractor.extract(
				Paths.get(assetsPath,libName),
				libZip);
		ZipUtil.decompress(libZip, libZip.getParent());
		
		Integer port = PortUtil.getNextFreePort();
		
		appendToFile(initScript.toString(), 
				Arrays.asList(
						String.format("init %s %s", PathUnixifier.get(tempDir), port.toString())));
		
		List<String> args = new ArrayList<>(baseArgs);
		
		args.add("-e");
		args.add(PathUnixifier.get(initScript));
		
		if(sessionFile.isPresent()){
			Path sessionPath = sessionFile.get();
			appendToFile(initScript.toString(),
					Arrays.asList(
							String.format(
									"loadSession %s",
									PathUnixifier.get(sessionPath))));
			args.add("-args");
			args.add(PathUnixifier.get(sessionPath.getParent()));
		}
		
		AnswerProtocol answerProtocol = new DefaultAnswerProtocol();
		ExternalApplication app = 
				new ExternalApplicationImpl(
						port,
						args.toArray(new String[0]),
						answerProtocol);
		app.start();
		return app;
	}
	
    private void appendToFile(String fileName, List<String> lines) throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(fileName, true));
        for(String line:lines){
        	w.write(line);
        	w.newLine();
        }        
        w.close();
    }
    
}
