package com.tcb.netmap.structureViewer.chimera;

import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.util.translator.ChainTranslator;
import com.tcb.netmap.util.translator.SingleTranslator;
import com.tcb.netmap.util.translator.Translator;

public class ChimeraFileTypeTranslator extends ChainTranslator {

	public ChimeraFileTypeTranslator() {
		super(getTranslators());
	}

	private static List<Translator> getTranslators() {
		return Arrays.asList(
				new SingleTranslator(Arrays.asList("parm7"),"prmtop"),
				new SingleTranslator(Arrays.asList("netcdf"), "nc")
				);
	}
			
}
