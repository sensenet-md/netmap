package com.tcb.netmap.structureViewer.chimera;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.command.AbstractCommander;
import com.tcb.netmap.external.command.Command;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.external.command.TclCommand;
import com.tcb.netmap.external.command.TclList;
import com.tcb.netmap.structureViewer.pymol.PymolCommander;

public class ChimeraCommander extends PymolCommander {

	public ChimeraCommander(ExternalApplication app){
		super(app);
	}
		
}
