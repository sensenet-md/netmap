package com.tcb.netmap.structureViewer.chimera;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationImpl;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerProtocol;
import com.tcb.netmap.external.answer.DefaultAnswerProtocol;
import com.tcb.netmap.util.PortUtil;

public class ChimeraStarter implements ExternalApplicationStarter {
	
	private static final String initScriptName = "init_viewer.py";
	private static final String libName = "chimera.zip";
	private static final String resourcePath = "chimera";
	private static final String assetsPath = "assets";
	private static final String tempPrefix = "chimera";

	private List<String> baseArgs;
	
	public ChimeraStarter(List<String> args){
		this.baseArgs = args;
	}
	
	@Override
	public ExternalApplication start() throws IOException {
		Path tempDir = new TempUtil(tempPrefix).createTempDir();
		Path initScript = Paths.get(tempDir.toString(), initScriptName);
		Path libZip = Paths.get(tempDir.toString(), libName);
		ResourceExtractor extractor = new ResourceExtractor(tempPrefix);
		
		extractor.extract(
				Paths.get(resourcePath, initScriptName),
				initScript);
		extractor.extract(
				Paths.get(assetsPath,libName),
				libZip);
		ZipUtil.decompress(libZip, libZip.getParent());
		
		Integer port = PortUtil.getNextFreePort();
		
		List<String> args = new ArrayList<>(baseArgs);
		args.add("--script");
		args.add(String.format("%s %s %d",initScript.toString(), initScript.toString(), port));
				
		AnswerProtocol answerProtocol = new DefaultAnswerProtocol();
		ExternalApplication app = new ExternalApplicationImpl(
				port, 
				args.toArray(new String[0]), 
				answerProtocol);
		app.start();
		return app;
	}
	

}
