package com.tcb.netmap.structureViewer.pymol;

import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.util.translator.ChainTranslator;
import com.tcb.netmap.util.translator.SingleTranslator;
import com.tcb.netmap.util.translator.Translator;

public class PymolFileTypeTranslator extends ChainTranslator {

	public PymolFileTypeTranslator() {
		super(getTranslators());
	}

	private static List<Translator> getTranslators() {
		return Arrays.asList(
				new SingleTranslator(Arrays.asList("prmtop"),"top"),
				new SingleTranslator(Arrays.asList("nc"), "netcdf")
				);
	}
	
	
}
