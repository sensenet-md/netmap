package com.tcb.netmap.structureViewer.pymol;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.netmap.structureViewer.Selecter;

public class PymolSelecter implements Selecter {

	private static final String delimiter = " | ";
	
	public String select(Atom atom) {
		Residue residue = atom.getResidue();
		String residueSelection = select(residue);
		String name = atom.getName();
		String sel = String.format(
				"(%s & name %s)",residueSelection,name);
		return sel;
	}

	public String select(Residue residue) {
		String chain = residue.getChain();
		Integer resId = residue.getIndex();
		String altLoc = residue.getAltLoc();
		String resInsert = residue.getResidueInsert();
		String sel = String.format(
				"(chain %s & resi %s & alt '%s')",
				chain,
				resId.toString()+resInsert,
				altLoc);
		return sel;
	}

	public String select(Interaction interaction) {
		String sourceSel = select(interaction.getSourceAtom());
		String targetSel = select(interaction.getTargetAtom());
		String sel = String.format("(%s + %s)",sourceSel,targetSel);
		return sel;
	}
	
	private String joinSelections(List<String> selections){
		String s = String.join(delimiter, selections);
		return "(" + s + ")";
	}
	
	@Override
	public String selectAtoms(Collection<Atom> atoms) {
		return joinSelections(
				atoms.stream()
				.map(a -> select(a))
				.collect(Collectors.toList()));
	}

	@Override
	public String selectResidues(Collection<Residue> residues) {
		return joinSelections(
				residues.stream()
				.map(r -> select(r))
				.collect(Collectors.toList()));
	}

	@Override
	public String selectInteractions(Collection<Interaction> interactions) {
		return joinSelections(
				interactions.stream()
				.map(i -> select(i))
				.collect(Collectors.toList()));
	}

	@Override
	public String selectAll() {
		return "all";
	}

}
