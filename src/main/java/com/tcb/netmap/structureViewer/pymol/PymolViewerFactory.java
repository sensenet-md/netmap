package com.tcb.netmap.structureViewer.pymol;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerParser;
import com.tcb.netmap.external.answer.DefaultAnswerParser;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.structureViewer.Selecter;
import com.tcb.netmap.structureViewer.StructureViewer;
import com.tcb.netmap.structureViewer.StructureViewerImpl;
import com.tcb.netmap.structureViewer.ViewerColorSerializer;
import com.tcb.netmap.structureViewer.ViewerFactory;
import com.tcb.netmap.structureViewer.vmd.VMDColorSerializer;
import com.tcb.netmap.structureViewer.vmd.VMDCommander;
import com.tcb.netmap.structureViewer.vmd.VMDFileTypeTranslator;
import com.tcb.netmap.structureViewer.vmd.VMDSelecter;
import com.tcb.netmap.structureViewer.vmd.VMDStarter;
import com.tcb.netmap.util.PathUnixifier;
import com.tcb.netmap.util.translator.Translator;

public class PymolViewerFactory implements ViewerFactory {

	private List<String> args;
	private Integer maxShownResidues;
	private Integer maxShownInteractions;
	private Optional<Path> sessionPath;

	public PymolViewerFactory(
			List<String> args,
			Integer maxShownResidues,
			Integer maxShownInteractions,
			Optional<Path> sessionPath){
		this.maxShownResidues = maxShownResidues;
		this.maxShownInteractions = maxShownInteractions;
		this.args = args;
		this.sessionPath = sessionPath;
	}
	
	public PymolViewerFactory(List<String> args){
		this(args,Integer.MAX_VALUE,Integer.MAX_VALUE,Optional.empty());
	}
	
	@Override
	public StructureViewer createViewer() throws IOException {
		ExternalApplication app = startApp();
		Selecter selecter = new PymolSelecter();
		Commander commander = new PymolCommander(app);
		ViewerColorSerializer colorSerializer = new PymolColorSerializer();
		Translator fileTypeTranslator = new PymolFileTypeTranslator();
		AnswerParser answerParser = new DefaultAnswerParser();
		return new StructureViewerImpl(
				app, selecter,
				commander, colorSerializer,
				fileTypeTranslator, answerParser,
				maxShownResidues, maxShownInteractions);
	}
	
	private ExternalApplication startApp() throws IOException {
		List<String> finalArgs = new ArrayList<>(args);
		if(sessionPath.isPresent()){
			finalArgs.add(PathUnixifier.get(sessionPath.get()));
		}
		ExternalApplicationStarter starter = new PymolStarter(finalArgs);
		ExternalApplication app = starter.start();
		return app;
	}
}
