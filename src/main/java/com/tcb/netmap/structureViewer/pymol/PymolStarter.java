package com.tcb.netmap.structureViewer.pymol;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.cytoscape.cyLib.util.ResourceExtractor;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.ExternalApplicationImpl;
import com.tcb.netmap.external.ExternalApplicationStarter;
import com.tcb.netmap.external.answer.AnswerProtocol;
import com.tcb.netmap.external.answer.DefaultAnswerProtocol;
import com.tcb.netmap.util.PortUtil;

public class PymolStarter implements ExternalApplicationStarter {

	private static final String resourcePath = "pymol";
	private static final String initScriptName = "init_viewer.py";
	
	private static final String assetsPath = "assets";
	private static final String libName = "pymol.zip";
	private static final String tempPrefix = "pymol";
	
	private List<String> baseArgs;
	
	public PymolStarter(List<String> args){
		this.baseArgs = args;
	}
	
	
	@Override
	public ExternalApplication start() throws IOException {
		Path tempDir = new TempUtil(tempPrefix).createTempDir();
		Path initScript = Paths.get(tempDir.toString(), initScriptName);
		Path libZip = Paths.get(tempDir.toString(), libName);
		ResourceExtractor extractor = new ResourceExtractor(tempPrefix);
		
		extractor.extract(
				Paths.get(resourcePath, initScriptName),
				initScript);
		extractor.extract(
				Paths.get(assetsPath,libName),
				libZip);
		ZipUtil.decompress(libZip, libZip.getParent());
		
		Integer port = PortUtil.getNextFreePort();

		List<String> args = new ArrayList<>(baseArgs);
		args.add("-r");
		args.add(initScript.toString());
		args.add("--");
		args.add(initScript.toString());
		args.add(port.toString());
				
		AnswerProtocol answerProtocol = new DefaultAnswerProtocol();
		ExternalApplication app = new ExternalApplicationImpl(port, args.toArray(new String[0]), answerProtocol);
		app.start();
		return app;
	}

}
