package com.tcb.netmap.structureViewer.pymol;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.netmap.external.ExternalApplication;
import com.tcb.netmap.external.command.AbstractCommander;
import com.tcb.netmap.external.command.Command;
import com.tcb.netmap.external.command.Commander;
import com.tcb.netmap.external.command.PythonCommand;
import com.tcb.netmap.external.command.PythonList;
import com.tcb.netmap.external.command.TclCommand;
import com.tcb.netmap.external.command.TclList;

public class PymolCommander extends AbstractCommander {

	public PymolCommander(ExternalApplication app){
		super(app);
	}
	
	@Override
	protected Command getCommand(String cmd, String... args){
		return new PythonCommand(cmd,args);
	}
	
	@Override
	public String stringify(List<String> lst) {
		return PythonList.translate(lst);
	}
	
}
