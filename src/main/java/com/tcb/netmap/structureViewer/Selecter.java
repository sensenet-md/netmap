package com.tcb.netmap.structureViewer;

import java.util.Collection;
import java.util.List;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;




public interface Selecter {
	public String selectAtoms(Collection<Atom> atoms);
	public String selectResidues(Collection<Residue> residues);
	public String selectInteractions(Collection<Interaction> interactions);
	public String selectAll();
}
