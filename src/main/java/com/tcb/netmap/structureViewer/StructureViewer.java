package com.tcb.netmap.structureViewer;

import java.awt.Color;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.netmap.util.limiter.TooManyItemsException;




public interface StructureViewer {
	
	public void stopConnection();
	public void kill();
	public Boolean isActive();
	public List<String> getModels() throws IOException;
	public void showModel(String model, Collection<Color> chainColors) throws IOException;
	public void showModel(String model) throws IOException;
	public void hideModel(String model) throws IOException;
	public void loadModel(String path, String format, String model) throws IOException;
	public void loadTraj(String model, String path, String format) throws IOException;
	public void deleteModel(String model) throws IOException;
	public void center(String model) throws IOException;
	public void zoomModel(String model) throws IOException;
	public void zoomResidues(String model, Collection<Residue> residues) throws IOException;
	public void zoomInteractions(String model, Collection<Interaction> interactions) throws IOException;
	public void undoZoom() throws IOException;
	public void setFrame(String model, Integer n) throws IOException;
	public void colorResidues(String model, Collection<Residue> residues, Color color) throws IOException;
	public void resetColors(String model) throws IOException;
	
	public void showResidues(String model, Collection<Residue> residues, Color color) throws TooManyItemsException, IOException;
	public void showResidues(String model, Collection<Residue> residues) throws TooManyItemsException, IOException;
	public void hideResidues(String model, Collection<Residue> residues) throws IOException;
	public void showInteractions(String model, Collection<Interaction> interactions, Color color) throws TooManyItemsException, IOException;
	public void showInteractions(String model, Collection<Interaction> interactions) throws TooManyItemsException, IOException;
	public void hideInteractions(String model, Collection<Interaction> interactions) throws IOException;
	
	public void setMaxShownResidues(Integer max);
	public void setMaxShownInteractions(Integer max);
	public Integer getMaxShownResidues(Integer max);
	public Integer getMaxShownInteractions(Integer max);
	
	public default List<Color> getDefaultChainColors(){
		return Arrays.asList(
				Color.GRAY,
				Color.YELLOW,
				Color.BLACK,
				Color.WHITE
		);
	};
	
	public default Color getDefaultSelectedColor(){
		return Color.ORANGE;
	}
	
	public default List<Color> getDefaultStickColors(){
		return getDefaultChainColors();
	}
	
	public default Color getDefaultInteractionColor(){
		return Color.GREEN;
	}
}
