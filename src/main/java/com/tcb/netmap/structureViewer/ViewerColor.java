package com.tcb.netmap.structureViewer;

import java.awt.Color;

public enum ViewerColor {
	RED, BLUE, GREEN, YELLOW, BLACK, WHITE, ORANGE, CYAN, GRAY;
}
