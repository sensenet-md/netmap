package com.tcb.netmap.structureViewer;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

public interface ViewerColorSerializer {
	public String getColorString(Color color);	
}
