package com.tcb.netmap.fileFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.tcb.netmap.util.translator.ChainTranslator;
import com.tcb.netmap.util.translator.Translator;

public class FormatCollectionImpl implements FormatCollection {

	private List<FileFormat> options;
	
	public FormatCollectionImpl(List<FileFormat> options){
		this.options = options;
	}
		
	@Override
	public Optional<FileFormat> search(String extension) {
		for(FileFormat f:options){
			String standardName = f.getStandardName();
			if(standardName.equals(extension)){
				return Optional.of(f);
			}
		}
		return Optional.empty();
	}

	@Override
	public List<FileFormat> getOptions() {
		return options;
	}

		
}
