package com.tcb.netmap.fileFormat.chimera;

import static com.tcb.netmap.fileFormat.StructureFileFormat.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollectionImpl;


public class ChimeraStructureFormatCollection extends FormatCollectionImpl {

	private static final List<FileFormat> options;
	
	static {
		options = Arrays.asList(
				PDB, MOL2, GRO, XYZ);
	}
	
	public ChimeraStructureFormatCollection() {
		super(options);
	}

}
