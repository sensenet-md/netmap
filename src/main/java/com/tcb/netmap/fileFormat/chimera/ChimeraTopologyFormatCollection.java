package com.tcb.netmap.fileFormat.chimera;

import static com.tcb.netmap.fileFormat.TopologyFileFormat.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollection;
import com.tcb.netmap.fileFormat.FormatCollectionImpl;
import com.tcb.netmap.fileFormat.StructureFileFormat;
import com.tcb.netmap.fileFormat.TopologyFileFormat;
import com.tcb.netmap.fileFormat.TrajectoryFileFormat;
import com.tcb.common.util.SafeMap;

public class ChimeraTopologyFormatCollection extends FormatCollectionImpl {
	
	private static final List<FileFormat> options;
	
	static {
		options = Arrays.asList(
				PRMTOP, PSF, TPR
				);
	}
		
	
	public ChimeraTopologyFormatCollection() {
		super(options);
	}

	
	
	

}
