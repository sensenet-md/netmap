package com.tcb.netmap.fileFormat;

public enum TopologyFileFormat implements FileFormat {
	PRMTOP, TPR, PSF;

	@Override
	public String getStandardName() {
		return this.name().toLowerCase();
	}

	@Override
	public String getDisplayedName() {
		return getStandardName();
	}
	
	@Override
	public String toString(){
		return getDisplayedName();
	}
}
