package com.tcb.netmap.fileFormat;

import java.util.List;
import java.util.Optional;
import java.util.Set;


public interface FileFormat {
	public String getStandardName();
	public String getDisplayedName();

}
