package com.tcb.netmap.fileFormat;

public enum TrajectoryFileFormat implements FileFormat {
	NC,XTC,TRR,DCD;

	@Override
	public String getStandardName() {
		return this.name().toLowerCase();
	}

	@Override
	public String getDisplayedName() {
		switch(this){
		case NC: return "nc (Netcdf)";
		//$CASES-OMITTED$
		default: return getStandardName();
		}
	}
	
	@Override
	public String toString(){
		return getDisplayedName();
	}
}
