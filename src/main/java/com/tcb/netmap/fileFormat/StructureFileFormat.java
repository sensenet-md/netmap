package com.tcb.netmap.fileFormat;

public enum StructureFileFormat implements FileFormat {
	PDB, MOL2, GRO, XYZ;

	@Override
	public String getStandardName() {
		return this.name().toLowerCase();
	}

	@Override
	public String getDisplayedName() {
		return getStandardName();
	}
	
	@Override
	public String toString(){
		return getDisplayedName();
	}
}
