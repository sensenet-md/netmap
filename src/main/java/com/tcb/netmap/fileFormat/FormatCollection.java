package com.tcb.netmap.fileFormat;

import java.util.List;
import java.util.Optional;

public interface FormatCollection {
	public Optional<FileFormat> search(String extension);
	public List<FileFormat> getOptions();
}
