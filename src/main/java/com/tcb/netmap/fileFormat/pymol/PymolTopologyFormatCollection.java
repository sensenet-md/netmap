package com.tcb.netmap.fileFormat.pymol;

import static com.tcb.netmap.fileFormat.TopologyFileFormat.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollectionImpl;
import com.tcb.netmap.fileFormat.StructureFileFormat;
import com.tcb.netmap.fileFormat.TopologyFileFormat;
import com.tcb.netmap.fileFormat.TrajectoryFileFormat;

public class PymolTopologyFormatCollection extends FormatCollectionImpl {

	private static final List<FileFormat> options;
	
	static {
		options = new ArrayList<>();
		options.addAll(new PymolStructureFormatCollection().getOptions());
		options.addAll(Arrays.asList(
				PRMTOP
				));
	}
	
	public PymolTopologyFormatCollection() {
		super(options);
	}

}
