package com.tcb.netmap.fileFormat.pymol;

import static com.tcb.netmap.fileFormat.StructureFileFormat.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollectionImpl;
import com.tcb.netmap.fileFormat.StructureFileFormat;
import com.tcb.netmap.fileFormat.TopologyFileFormat;
import com.tcb.netmap.fileFormat.TrajectoryFileFormat;

public class PymolStructureFormatCollection extends FormatCollectionImpl {

	private static final List<FileFormat> options;
	
	static {
		options = Arrays.asList(
				PDB, MOL2, GRO, XYZ);
	}
	
	public PymolStructureFormatCollection() {
		super(options);
	}

}
