package com.tcb.netmap.fileFormat.pymol;

import static com.tcb.netmap.fileFormat.TrajectoryFileFormat.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tcb.netmap.fileFormat.FileFormat;
import com.tcb.netmap.fileFormat.FormatCollectionImpl;
import com.tcb.netmap.fileFormat.StructureFileFormat;
import com.tcb.netmap.fileFormat.TopologyFileFormat;
import com.tcb.netmap.fileFormat.TrajectoryFileFormat;

public class PymolTrajectoryFormatCollection extends FormatCollectionImpl {

	private static final List<FileFormat> options;
	
	static {
		options = new ArrayList<>();
		options.addAll(Arrays.asList(
				DCD,XTC,TRR
				));
	}
	
	public PymolTrajectoryFormatCollection() {
		super(options);
	}

}
