
variable logFile
set ::argv $argv

proc log {text} {
    global logFile
    set fileId [open $logFile [list WRONLY APPEND CREAT]]
    puts $fileId $text
    #puts $text
    close $fileId
}

proc handleComm {sock} {
    global errorInfo
    set message [gets $sock]
    log "Got message: $message"
    if { [catch {
	set result [eval $message]
        set answer "0$result"}]
     } {
	set err [lindex [split $errorInfo "\n"] 0]
	set answer "1$err" }
    log "Answer: $answer"
    puts $sock $answer
    flush $sock
}       

proc connect {port} {
     log "Opening connection on port $port"
     set sock [socket localhost $port]
     fileevent $sock readable \
         [list handleComm $sock]          
}

proc loadLibs {scriptDir} {
    source [file join $scriptDir vmd_commander vmd-commander.tcl]
}

proc initLog {scriptDir} {
    global logFile
    set logFile [file join $scriptDir "socket.log"]
}

proc init {scriptDir port} {
    initLog $scriptDir
    loadLibs $scriptDir
    connect $port
}

proc loadSession {sessionFile} {
    source $sessionFile
}
