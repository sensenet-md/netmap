#!/usr/bin/env python3

import os,sys

scriptDir = os.path.dirname(sys.argv[1])
sys.path.append(scriptDir)

import socket
import errno
from socket import error as socketError
from pymol_commander.pymol_commander import PymolCommander
from pymol import CmdException,stored
from time import sleep
from _thread import start_new_thread
import logging

port = int(sys.argv[2])

logger = logging.getLogger(__name__)
fh = logging.FileHandler(os.path.join(scriptDir,__name__ + '.log'))
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(logging.DEBUG)

def accept(commander, command):
    answer = ''
    error = ''
    try:
        logger.debug("Running command")
        logger.debug(command)
        answer = eval('commander.' + command)
        logger.debug("Finished command")
    except:
        error = sys.exc_info()[0]
        logger.error(error)

    if error:
        return "1" + str(error)
    
    if not answer:
        answer = ''
    
    return "0" + answer


def blockRecv(socket):
    data = []
    while True:
        try:
            char = socket.recv(1)
            if not char: raise RuntimeError("Socket closed")
            data.append(char.decode('utf-8'))
        except socketError as e:
            if e.errno == errno.EINTR:
                logger.error("Caught EINTR exception")
                continue
            else:
                raise e
        if len(data) > 0 and data[-1] == '\n':
            return ''.join(data)


def run():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    commander = PymolCommander()
    stored.commander = commander

    sock.connect(('localhost',port))

    print("Opened socket on port {}".format(port)) 
    try:
        while True:
            message = blockRecv(sock)
            logger.debug("Received message: " + message)
            answer = accept(commander, message)
            logger.debug("Answering: " + answer)
            answer = answer + '\n'
            sock.sendall(answer.encode())
    except:
            exType,exValue,_ = sys.exc_info()
            logger.error("Caught exception: {}, message: {}".format(exType,exValue))

start_new_thread(run,())

