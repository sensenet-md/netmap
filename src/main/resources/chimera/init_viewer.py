#!/usr/bin/env python2

import os,sys

print sys.argv
scriptDir = os.path.dirname(sys.argv[1])
sys.path.append(scriptDir)

import socket
import errno
from socket import error as socketError
from chimera_commander.chimera_commander import ChimeraCommander
from time import sleep
from thread import start_new_thread
import logging

port = int(sys.argv[2])

logger = logging.getLogger(__name__)
fh = logging.FileHandler(os.path.join(scriptDir,__name__ + '.log'))
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(logging.DEBUG)

def accept(commander, command):
    answer = ''
    error = ''
    try:
        logger.debug("Running command")
        logger.debug(command)
        answer = eval('commander.' + command)
        logger.debug("Finished command")
    except Exception as error:
        logger.error(error.message)

    if error:
        return "1" + error.message
    
    if not answer:
        answer = ''
    
    return "0" + answer


def blockRecv(socket):
    data = []
    while True:
        try:
            char = socket.recv(1)
            if not char: raise RuntimeError("Socket closed")
            data.append(char)
        except socketError as e:
            if e.errno == errno.EINTR:
                logger.error("Caught EINTR exception")
                continue
            else:
                raise e
        if len(data) > 0 and data[-1] == '\n':
            return ''.join(data)


def run():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    commander = ChimeraCommander()
    chimera.commander = commander

    sock.connect(('localhost',port))

    print "Opened socket on port {}".format(port) 
    try:
        while True:
            message = blockRecv(sock)
            logger.debug("Received message: " + message)
            answer = accept(commander, message)
            logger.debug("Answering: " + answer)
            sock.sendall(answer + '\n')
    except Exception as e:
            logger.error("Caught exception: " + e.message)


start_new_thread(run,())

