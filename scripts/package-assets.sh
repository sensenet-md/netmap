#!/bin/bash

set -eu
set -o pipefail

ProjDir=$(realpath $(dirname ${BASH_SOURCE[0]})/..)

AssetDir=${ProjDir}/src/assets
TargetDir=${ProjDir}/src/main/resources/assets

Dirs=$(find ${AssetDir} -mindepth 1 -maxdepth 1 -type d)

rm -rfv ${TargetDir}
mkdir ${TargetDir}

for Dir in ${Dirs}
do
        Name=$(basename ${Dir})
        cd ${Dir}
        zip -r ${TargetDir}/${Name}.zip * -x \*.git\* \*.pyc \*.idea\* \*.swp \*~
        cd -
done

